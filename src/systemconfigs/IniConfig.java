/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package systemconfigs;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import logback.LogBackLogger;

/**
 *
 * @author ccm
 */
public class IniConfig {

    public String getPulseDBPath() {
        return pulseDBPath;
    }
    public void setPulseDBPath(String path) {
        pulseDBPath = path;
    }
    
    public double getPulsePCT() {
        return pulsePCT;
    }

    public void setPulsePCT(double pulsePCT) {
        this.pulsePCT = pulsePCT;
    }

    public int getPulseConst() {
        return pulseConst;
    }

    public void setPulseConst(int pulseConst) {
        this.pulseConst = pulseConst;
    }

    public String getGemsIP() {
        return gemsIP;
    }

    public void setGemsIP(String gemsIP) {
        this.gemsIP = gemsIP;
    }

    public int getGemsPort() {
        return gemsPort;
    }

    public void setGemsPort(int gemsPort) {
        this.gemsPort = gemsPort;
    }

    public String getGemsCustomerID() {
        return gemsCustomerID;
    }

    public void setGemsCustomerID(String gemsCustomerID) {
        this.gemsCustomerID = gemsCustomerID;
    }

    public int getGemsDuration() {
        return gemsDuration;
    }

    public void setGemsDuration(int gemsDuration) {
        this.gemsDuration = gemsDuration;
    }

    public int getGemsCount() {
        return gemsCount;
    }

    public void setGemsCount(int gemsCount) {
        this.gemsCount = gemsCount;
    }
    
    public String getProtocolVer() {
        return protocolVer;
    }
    
    public void setProtocolVer(String ver) {
        protocolVer = ver;
    }
    
    public String getGemsMobile() {
        return gemsMobile;
    }
    
    public void setGemsMobile(String mobile) {
        this.gemsMobile = mobile;
    }
    
    private String pulseDBPath;
    
    private double pulsePCT;
    private int pulseConst;

    private String gemsIP;
    private int gemsPort;
    private String gemsCustomerID;
    private String gemsMobile;
    
    private String protocolVer;
    
    private int gemsDuration;
    private int gemsCount;
    
    public String readConfigIni_PulseMeter() {
    try {
        Properties p = new Properties();
        p.load(new FileInputStream("/etc/gateway/config.ini"));

        String ctpath = p.getProperty("PULSE_DB_PATH");
        this.pulseDBPath = ctpath;
        
        String ctip = p.getProperty("PULSE_GEMSIP");
        this.gemsIP = ctip;
        String ctport = p.getProperty("PULSE_GEMSPORT");
        this.gemsPort = Integer.parseInt(ctport);
        String ctid = p.getProperty("PULSE_GEMSID");
        this.gemsCustomerID = ctid;
        String ver = p.getProperty("PULSE_GEMSPROTOCOL");
        this.protocolVer = ver;
        String mobile = p.getProperty("PULSE_GEMSMOBILE");
        this.gemsMobile = mobile;              
        
        
        String pulsepct = p.getProperty("PULSE_PCT");
        this.pulsePCT = Double.parseDouble(pulsepct);
        String pulseconst = p.getProperty("PULSE_CONST");
        this.pulseConst = Integer.parseInt(pulseconst);
        
        String duration = p.getProperty("PULSE_GEMSDURATION");
        this.gemsDuration = Integer.parseInt(duration);
        String count = p.getProperty("PULSE_GEMSCOUNT");
        this.gemsCount = Integer.parseInt(count);
        
        return "OK";
    } catch (IOException ex) {
        LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//        Logger.getLogger(IniConfig.class.getName()).log(Level.SEVERE, null, ex);
    }
    return "FAIL";
    }    
    
    public String writeConfigIni_PulseMeter() {
        try {
            Properties p = new Properties();
            p.load(new FileInputStream("/etc/gateway/config.ini"));

            p.setProperty("PULSE_DB_PATH", pulseDBPath);
            
            p.setProperty("PULSE_GEMSIP", this.gemsIP);
            p.setProperty("PULSE_GEMSPORT", String.valueOf(this.gemsPort));
            p.setProperty("PULSE_GEMSID", this.gemsCustomerID);
            p.setProperty("PULSE_GEMSPROTOCOL", this.protocolVer);
            p.setProperty("PULSE_GEMSMOBILE", this.gemsMobile);
            
            p.setProperty("PULSE_PCT", String.valueOf(this.pulsePCT));
            p.setProperty("PULSE_CONST", String.valueOf(this.pulseConst));
            
            p.setProperty("PULSE_GEMSDURATION", String.valueOf(this.gemsDuration));
            p.setProperty("PULSE_GEMSCOUNT", String.valueOf(this.gemsCount));
            
            SimpleDateFormat simdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            p.store(new FileOutputStream("/etc/gateway/config.ini"), "GATEWAY CONFIG.INI - last [" + simdf.format(new Date()) + "]");

            return "OK";
        } catch (IOException ex) {
            LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//            Logger.getLogger(IniConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "FAIL";
    }
}
