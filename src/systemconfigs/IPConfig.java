/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package systemconfigs;

import misc.Shell;

/**
 *
 * @author ccm
 */
public class IPConfig {
    private String ipaddr;
    private String netmask;
    private String gateway;

    public String getIpaddr()
    {
      return this.ipaddr;
    }
    public void setIpaddr(String ipaddr) {
      this.ipaddr = ipaddr;
    }

    public String getNetmask() {
      return this.netmask;
    }
    public void setNetmask(String netmask) {
      this.netmask = netmask;
    }

    public String getGateway() {
      return this.gateway;
    }
    public void setGateway(String gateway) {
      this.gateway = gateway;
    }

    public void changeStaticIPAddress() throws Exception {
      Shell shell = new Shell();
      shell.setStaticEthernet("eth0", this.ipaddr, this.netmask, this.gateway, "168.126.63.1");
    }
    public void changeDHCPIPAddress() throws Exception {
      Shell shell = new Shell();
      shell.setDHCPEthernet("eth0");
    }    
}
