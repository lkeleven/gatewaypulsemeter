/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socketiomodule;

import gemsprotocol.GEMSProtocolAuthData;
import gemsprotocol.GEMSProtocolCalculateData;
import gemsprotocol.GEMSProtocolCountData;
import gemsprotocol.GEMSProtocolDurationData;
import gemsprotocol.GEMSProtocolHourMeterData;
import gemsprotocol.GEMSProtocolStatusData;
import gemsprotocol.GEMSProtocolPacketMaker;
import gemsprotocol.GEMSProtocolMeterData;
import gemsprotocol.GEMSProtocolResponser;
import gemsprotocol.GEMSProtocolTimeMeterData;
import gemsprotocol.GEMSProtocolTimeOffsetData;
import gemsprotocol.GEMSProtocolTypes;
import gemsprotocol.GEMSProtocolTypes.GEMSResponseType;
import gemsprotocol.GEMSUtils;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdbcmodules.JDBCDerbyConnection;
import jdbcmodules.JDBCPulseMeter;
import logback.LogBackLogger;
import meterdatamodels.MeterRealDatas;
import meterdatamodels.MeterRealDatas.MeterRealData;
import socketiomodule.GEMSTCPMessages.GEMSTCPMessage;

/**
 *
 * @author ccm
 */
public class GEMSTCPClient {
    private final JDBCPulseMeter jdbcPulseMeter = new JDBCPulseMeter();
    private JDBCDerbyConnection derbyCon;
    public void setDerbyConnection(JDBCDerbyConnection con) {
        derbyCon = con;
        jdbcPulseMeter.setConnection(derbyCon.getDBConnection());
    }
        
    private TCPClientIO tcpio = null;
    
    private String gemsServerIp;
    private int gemsServerPort;
    public void setGEMSServerIP(String ip) {
        gemsServerIp = ip;
    }
    public String getGEMSServerIP() {
        return gemsServerIp;
    }
    public void setGEMSServerPort(int port) {
        gemsServerPort = port;
    }
    public int getGEMSServerPort() {
        return gemsServerPort;
    }
    
    
    private String protocolVer;
    public void setProtocolVer(String ver) {
        protocolVer = ver;
        gemsHeader.setProtocolVer(protocolVer);
    }
    public String getProtocolVer() {
        return protocolVer;
    }
    
    
    private String mobileNum;
    public void setMobileNum(String num) {
        mobileNum = num;
        gemsHeader.setMobileNum(num);
    }
    public String getMobileNum() {
        return mobileNum;
    }
    
    
    private String customerId;
    public void setCustomerId(String id) {
        customerId = id;
    }
    public String getCustomerId() {
        return customerId;
    }
    
    private byte[] serviceKey;
    public void setServiceKey(byte[] key) {
        serviceKey = key;
        gemsHeader.setServiceKey(key);
    }
    public byte[] getServiceKey() {
        return serviceKey;
    }
    
    private byte[] encryptedKey;
    public void setEncryptedKey(byte[] key) {
        encryptedKey = key;
        gemsHeader.setEncryptedKey(key);
    }
    public byte[] getEncryptedKey() {
        return encryptedKey;
    }
    
    ///
    private int duration;
    public void setDuration(int value) {
        duration = value;
    }
    public int getDuration() {
        return duration;                
    }
    
    private int meterRealCount;
    public void setMeterRealCount(int count) {
        meterRealCount = count;
    }
    public int getMeterRealCount() {
        return meterRealCount;
    }
    
    
    private float pulsePCT;
    public void setPulsePCT(float pct) {
        pulsePCT = pct;
    }
    public float getPulsePCT() {
        return pulsePCT;
    }
    
    private int pulseConst;
    public void setPulseConst(int value) {
        pulseConst = value;
    }
    public int getPulseConst() {
        return pulseConst;
    }
    //
    
    private final GEMSProtocolPacketMaker gemsHeader = new GEMSProtocolPacketMaker();
    public GEMSProtocolPacketMaker getGEMSProtocolHeader() {
        return gemsHeader;
    }
    
    private GEMSTCPClientListener gemsClientListener = null;
    public void setGEMSTCPClientListener(GEMSTCPClientListener lisnter) {
        gemsClientListener = lisnter;
    }
    
    public GEMSTCPClient() {
        Timer secTimer = new Timer(true);
        secTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                while (gemsTcpMsgs.getDataCount() > 0) {
                    try {
                        GEMSTCPMessage msg = gemsTcpMsgs.getFirstMessage();
                        doWriteTCPMessage(msg);
                        
                        Thread.sleep(30);
                    } catch (InterruptedException ex) {
                        LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//                        Logger.getLogger(GEMSTCPClient.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }, 0L, 500L);
    } 
    
    private boolean createNewTcpClient()  //throws IOException
    {
        try {
            if (tcpio != null) {
              tcpio.close();
              tcpio = null;
            }
            tcpio = new TCPClientIO();
            tcpio.Open(gemsServerIp, gemsServerPort);
            
            return true;
        } catch (IOException ex) {
            LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//            Logger.getLogger(GEMSTCPClient.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    private void doWriteTCPMessage(GEMSTCPMessage message)  {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        LogBackLogger.getInstance().getPacketLogger().debug("   ***** " + message.getGEMSTCPMessageType() + " start *****");
        LogBackLogger.getInstance().getPacketLogger().debug("   ***** " + sdf.format(new Date()) + " *****");
        
//        LogBackLogger.getInstance().getSTDOUTLogger().info("   ***** " + message.getGEMSTCPMessageType() + " start *****");
//        LogBackLogger.getInstance().getSTDOUTLogger().info("   ***** " + sdf.format(new Date()) + " *****");
        
        if (!createNewTcpClient()) { return; }
        
        int timeout = 0;
        
        
        try {
        
            if (tcpio ==  null) return;
        
            tcpio.getIODataWriter().setTXDatas(message.getGEMSTCPMessageData());
            while (true)
            {
                if (timeout > 15) {
                    LogBackLogger.getInstance().getPacketLogger().debug("   !!!!! " + message.getGEMSTCPMessageType() + " Timeout !!!!! ");
                    break;
                }
                try
                {
                  Thread.sleep(100L);
                } catch (InterruptedException ex) {
                    LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
                }

                if (this.tcpio.getIODataReader().getRXDatas() > 0) {
                    GEMSProtocolResponser gemsProtocol = getGEMSProtocol(message);
                    gemsProtocol.setGEMSProtocolHeader(gemsHeader);
                    onGEMSResponseResult (gemsProtocol.setGEMSTCPProtocolResponse(tcpio.getIODataReader().getLastRXData()) );
                    break;
                }
                timeout++;
            }
            
            tcpio.close();  
            
        } catch (IOException ex) {
            LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//            Logger.getLogger(GEMSTCPClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        LogBackLogger.getInstance().getPacketLogger().debug("   ===== " + sdf.format(new Date()) + " =====");
        LogBackLogger.getInstance().getPacketLogger().debug("   ===== " + message.getGEMSTCPMessageType() + " end ===== ");
        
//        LogBackLogger.getInstance().getSTDOUTLogger().info("   ===== " + sdf.format(new Date()) + " =====");
//        LogBackLogger.getInstance().getSTDOUTLogger().info("   ===== " + message.getGEMSTCPMessageType() + " end ===== ");
    }
    
    private GEMSProtocolResponser getGEMSProtocol(GEMSTCPMessage message) {
        switch (message.getGEMSTCPMessageType()) {
            //case gmtAuth: GEMSProtocolAuthData auth = new GEMSProtocolAuthData(); return auth;
            case gmtStatus: GEMSProtocolStatusData status = new GEMSProtocolStatusData(); return status;
            case gmtMeter: GEMSProtocolMeterData meter = new GEMSProtocolMeterData(); return meter;
            case gmtTimeMeter: GEMSProtocolTimeMeterData timeMeter = new GEMSProtocolTimeMeterData(); return timeMeter;
            case gmtHourMeter: GEMSProtocolHourMeterData hourMeter = new GEMSProtocolHourMeterData(); return hourMeter; 
            case gmtDuration: return null;
            case gmtCount: return null;
            case gmtCalc: return null;
            case gmtTimeOffset: return null;
            
            default: 
                GEMSProtocolAuthData auth = new GEMSProtocolAuthData(); 
                auth.setGEMSProtocolHeader(gemsHeader);
                auth.setAuthDataListener(new GEMSProtocolAuthData.ProtocolAuthDataListener() {
                    @Override
                    public void onGetAuthDataRequest(byte[] serviceKey, byte[] encrypedKey) {
                        setServiceKey(serviceKey);
                        setEncryptedKey(encrypedKey);
                    }
                });
                return auth; 
        }
    }
    
    private final GEMSTCPMessages gemsTcpMsgs = new GEMSTCPMessages();
    private void addGEMSTCPMessage(GEMSTCPMessages.GEMSTCPMessageType type, byte[] data) {
        if (type.equals(GEMSTCPMessages.GEMSTCPMessageType.gmtAuth)) {
            gemsTcpMsgs.addMGEMSAuthData(data);
        } else {
            gemsTcpMsgs.addMeterRealData(type, data);
        }
        
    }
    
    public void getAuthDataMessage() throws IOException {
        LogBackLogger.getInstance().getPacketLogger().debug("     [[[[[ ADD getAuthDataMessage start ]]]]] ");
        GEMSProtocolAuthData auth = new GEMSProtocolAuthData();
        auth.setGEMSProtocolHeader(gemsHeader);
        auth.setCustomerId(customerId);
        
        addGEMSTCPMessage(GEMSTCPMessages.GEMSTCPMessageType.gmtAuth, auth.getAuthInfoRequest());
    }
    
    public void putStatusDataMessage(int event) throws IOException {
        LogBackLogger.getInstance().getPacketLogger().debug("     [[[[[ ADD putStatusDataMessage start ]]]]] ");
        GEMSProtocolStatusData status = new GEMSProtocolStatusData();
        status.setGEMSProtocolHeader(gemsHeader);
        
        addGEMSTCPMessage(GEMSTCPMessages.GEMSTCPMessageType.gmtStatus, status.putStatusDataRequest(event));
    }
    
    private MeterRealDatas localmrds;    
    public void putMeterDataMessage(MeterRealDatas mrds) throws IOException {
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        LogBackLogger.getInstance().getPacketLogger().debug("     [[[[[ ADD putMeterDataMessage start ]]]]] ");
        LogBackLogger.getInstance().getPacketLogger().debug("     [[[[[ " + sdf.format(new Date()) + " ]]]]]");
        
//        LogBackLogger.getInstance().getSTDOUTLogger().info("     [[[[[ ADD putMeterDataMessage start ]]]]] ");
//        LogBackLogger.getInstance().getSTDOUTLogger().info("     [[[[[ " + sdf.format(new Date()) + " ]]]]]");
        
        GEMSProtocolMeterData meter = new GEMSProtocolMeterData();
        meter.setGEMSCount(meterRealCount);
        meter.setGEMSDuration(duration);
        meter.setGEMSProtocolHeader(gemsHeader);
        
//        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
//        for (int i = 0; i<mrds.getDataCount(); i++) {
//            MeterRealData mrd = mrds.getMeterRealData(i);
//            String fvalue = String.format("%.2f", (mrd.getPulseCount() * pulsePCT / pulseConst) );
//            LogBackLogger.getInstance().getPacketLogger().debug("MeterRealData : " +  df.format(mrd.getPulsedate()) + " - " + mrd.getPulseCount() + " [" + fvalue + "] ");
//        }
        localmrds = mrds;
        
        addGEMSTCPMessage(GEMSTCPMessages.GEMSTCPMessageType.gmtMeter,meter.putMeterDataRequest(mrds,pulsePCT,pulseConst));
    }
    
    private boolean noPiggyAck = true;
    private boolean dataflag;
    private void onGEMSResponseResult(GEMSResponseType grtType) {
        LogBackLogger.getInstance().getPacketLogger().debug("     ***** Response Message start ***** ");
        LogBackLogger.getInstance().getPacketLogger().debug("     :: Response Type = " + grtType + " :: ");
        switch (grtType) {
            case grtAck:
                try {
                    if (noPiggyAck) { 
                        gemsTcpMsgs.removeFirstMessage(); 
                        if (dataflag) {
                            if (gemsTcpMsgs.getDataCount() > 0) {
                                if (gemsTcpMsgs.getFirstMessage().getGEMSTCPMessageType().equals(GEMSTCPMessages.GEMSTCPMessageType.gmtMeter)) {
                                    putMeterDataMessage(localmrds);                        
                                } else if (gemsTcpMsgs.getFirstMessage().getGEMSTCPMessageType().equals(GEMSTCPMessages.GEMSTCPMessageType.gmtStatus)) {
                                    putStatusDataMessage(1);
                                }
                                gemsTcpMsgs.removeFirstMessage();
                            }
                        }
                    }
                
                    Thread.sleep(100L);
                } catch (InterruptedException | IOException ex) {
                    LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//                    Logger.getLogger(GEMSTCPClient.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case grtAckPiggy:
                gemsTcpMsgs.removeFirstMessage();
                noPiggyAck = false;
                try {
                    Thread.sleep(100L);
                    if (tcpio.getIODataReader().getRXDatas() > 0) {
                        onGEMSProtocolPiggyBack(tcpio.getIODataReader().getLastRXData());
                    }
                } catch (IOException | InterruptedException ex) {
                    LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//                    Logger.getLogger(GEMSTCPClient.class.getName()).log(Level.SEVERE, null, ex);
                }  
                noPiggyAck = true;
                break;
            case grtNotAuth:
                try {
                    Thread.sleep(100L);
                    if (gemsTcpMsgs.getFirstMessage().getGEMSTCPMessageType().equals(GEMSTCPMessages.GEMSTCPMessageType.gmtAuth)) {
                        gemsTcpMsgs.removeFirstMessage();
                        dataflag = false;
                    } else {
                        dataflag = true;
                    }
                    getAuthDataMessage();
                } catch (IOException | InterruptedException ex) {
                    LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//                    Logger.getLogger(GEMSTCPClient.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case grtNack: 
                try {
                    Thread.sleep(100L);
                } catch (InterruptedException ex) {
                    LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//                    Logger.getLogger(GEMSTCPClient.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            default: break;
        }
    }
    
    private void onGEMSProtocolPiggyBack(byte[] data) throws IOException {
        //GEMSUtils.printBytes(tcpio.getIODataReader().getLastRXData());        
        
        byte gpttype = data[0];
        LogBackLogger.getInstance().getPacketLogger().debug(GEMSProtocolTypes.getGEMSPiggyType(gpttype).toString());
        switch ( GEMSProtocolTypes.getGEMSPiggyType(gpttype) ) {
            case gptTimeMeter: getTimeMeterDataMessage(data); break;
            case gptHourMeter: getHourMeterDataMessage(data); break;
            case gptDuration: setDurationDataMessage(data); break;
            case gptCount: setCountDataMessage(data); break;
            case gptCalculate: setCalculateDataMessage(data); break;
            case gptTimeOffset: setTimeOffsetDataMessage(data); break;
        }
    }
    
    private void getTimeMeterDataMessage(byte[] data) throws IOException {
        LogBackLogger.getInstance().getPacketLogger().debug("          >>>>> getTimeMeterDataMessage start <<<<< ");
        
        GEMSProtocolTimeMeterData timeMeter = new GEMSProtocolTimeMeterData();
        timeMeter.setGEMSProtocolHeader(gemsHeader);
        timeMeter.setGEMSDuration(duration);
        timeMeter.setJDBCPulseMeterSelect(jdbcPulseMeter);
        timeMeter.getTimeMeterDataRequest(data, pulsePCT, pulseConst);
                
        int timeout = 0;
        
        tcpio.getIODataWriter().setTXDatas(timeMeter.getTimeMeterDataRequest(data,pulsePCT,pulseConst));
        while (true)
        {
            if (timeout > 15) {
                LogBackLogger.getInstance().getPacketLogger().debug("          !!!!! getTimeMeterDataMessage Timeout !!!!! ");
                break;
            }
            try
            {
                Thread.sleep(100L);
            } catch (InterruptedException ex) {
                LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//                Logger.getLogger(GEMSTCPClient.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (this.tcpio.getIODataReader().getRXDatas() > 0) {
                onGEMSResponseResult(timeMeter.getTimeMeterDataResponse(tcpio.getIODataReader().getLastRXData()));
                break;
            }
            timeout++;
        }
        
        LogBackLogger.getInstance().getPacketLogger().debug("          <<<<< getTimeMeterDataMessage end >>>>> ");
    }
    
    private void getHourMeterDataMessage(byte[] data) throws IOException {
        LogBackLogger.getInstance().getPacketLogger().debug("          >>>>> getHourMeterDataMessage start <<<<< ");        
        
        GEMSProtocolHourMeterData hourMeter = new GEMSProtocolHourMeterData();
        hourMeter.setGEMSProtocolHeader(gemsHeader);
        hourMeter.setGEMSDuration(duration);
        hourMeter.setJDBCPulseMeterSelect(jdbcPulseMeter);
        
        int timeout = 0;
        
        tcpio.getIODataWriter().setTXDatas(hourMeter.getHourMeterDataRequest(data,pulsePCT,pulseConst));
        while (true)
        {
            if (timeout > 15) {
                LogBackLogger.getInstance().getPacketLogger().debug("          !!!!! getHourMeterDataMessage Timeout !!!!! ");
                break;
            }
            try
            {
                Thread.sleep(100L);
            } catch (InterruptedException ex) {
                LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//                Logger.getLogger(GEMSTCPClient.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (this.tcpio.getIODataReader().getRXDatas() > 0) {
                onGEMSResponseResult(hourMeter.getHourMeterDataResponse(tcpio.getIODataReader().getLastRXData()));
                break;
            }
            timeout++;
        }
        
        LogBackLogger.getInstance().getPacketLogger().debug("          <<<<< getHourMeterDataMessage end >>>>> ");
    }
    
    private void setDurationDataMessage(byte[] data) throws IOException {
        LogBackLogger.getInstance().getPacketLogger().debug("          >>>>> setDurationDataMessage start <<<<< ");        
        
        GEMSProtocolDurationData durationPK = new GEMSProtocolDurationData();
        durationPK.setGEMSProtocolHeader(gemsHeader);
        durationPK.setDurationDataListener(new GEMSProtocolDurationData.ProtocolDurationDataListener() {
            @Override
            public void onSetDurationDataRequest(short duration) {
                setDuration(duration);
                // Main Listener Duration 
                if (gemsClientListener != null) 
                    gemsClientListener.onSetDurationDataRequest(duration);
            }
        });
        
        tcpio.getIODataWriter().setTXDatas(durationPK.setDurationDataRequest(data));
        
        LogBackLogger.getInstance().getPacketLogger().debug("          <<<<< setDurationDataMessage end >>>>> ");
    }
    
    private void setCountDataMessage(byte[] data) throws IOException {
        LogBackLogger.getInstance().getPacketLogger().debug("          >>>>> setCountDataMessage start <<<<< ");        
        
        GEMSProtocolCountData countPK = new GEMSProtocolCountData();
        countPK.setGEMSProtocolHeader(gemsHeader);
        countPK.setCountDataListener(new GEMSProtocolCountData.ProtocolCountDataListener() {
            @Override
            public void onSetCountDataRequest(byte count) {
                setMeterRealCount(count);
                // Main Listener MeterRealCount
                if (gemsClientListener != null) 
                    gemsClientListener.onSetCountDataRequest(count);
            }
        });
        
        tcpio.getIODataWriter().setTXDatas(countPK.setCountDataRequest(data));
        
        LogBackLogger.getInstance().getPacketLogger().debug("          <<<<< setCountDataMessage end >>>>> ");
    }
    
    private void setCalculateDataMessage(byte[] data) throws IOException {
        LogBackLogger.getInstance().getPacketLogger().debug("          >>>>> setCalculateDataMessage start <<<<< ");        
        
        GEMSProtocolCalculateData calculatePK = new GEMSProtocolCalculateData();
        calculatePK.setGEMSProtocolHeader(gemsHeader);
        calculatePK.setCountDataListener(new GEMSProtocolCalculateData.ProtocolCalculateDataListener() {
            @Override
            public void onSetCalculateDataRequest(float pct, short constnum) {
                setPulsePCT(pct);
                setPulseConst(constnum);
                // Main Listener PulsePCT, PulseConst
                if (gemsClientListener != null) 
                    gemsClientListener.onSetCalculateDataRequest(pct, constnum);
            }
        });
        
        tcpio.getIODataWriter().setTXDatas(calculatePK.setCalculateDataRequest(data));
        
        LogBackLogger.getInstance().getPacketLogger().debug("          <<<<< setCalculateDataMessage end >>>>> ");
    }
    
    private void setTimeOffsetDataMessage(byte[] data) throws IOException {
        LogBackLogger.getInstance().getPacketLogger().debug("          >>>>> setTimeOffsetDataMessage start <<<<< ");        
        
        GEMSProtocolTimeOffsetData timeOffsetPK = new GEMSProtocolTimeOffsetData();
        timeOffsetPK.setGEMSProtocolHeader(gemsHeader);
        timeOffsetPK.setCountDataListener(new GEMSProtocolTimeOffsetData.ProtocolTimeOffsetDataListener() {
            @Override
            public void onSetTimeOffsetDataRequest(byte offset) {
                // Main Listener TimeOffset
                if (gemsClientListener != null) 
                    gemsClientListener.onSetTimeOffsetDataRequest(offset);
            }
        });
        
        tcpio.getIODataWriter().setTXDatas(timeOffsetPK.setTimeOffsetDataRequest(data));
        
        LogBackLogger.getInstance().getPacketLogger().debug("          <<<<< setTimeOffsetDataMessage end >>>>> ");
    }
    
}
