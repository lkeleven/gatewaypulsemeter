package socketiomodule;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import logback.LogBackLogger;

public class TCPClientIO
{
  private Socket socket = null;
  private String serverip;
  private int serverport;
  private IODataReader ioDataReader;
  private IODataWriter ioDataWriter;

  public String getServerIP()
  {
    return this.serverip;
  }
  public void setServerIP(String ip) {
    this.serverip = ip;
  }

  public int getServerPort() {
    return this.serverport;
  }
  public void setServerPort(int port) {
    this.serverport = port;
  }

  public IODataReader getIODataReader() {
    return this.ioDataReader;
  }
  public IODataWriter getIODataWriter() {
    return this.ioDataWriter;
  }

  public TCPClientIO() {
    this.socket = new Socket();
    this.ioDataReader = new IODataReader();
    this.ioDataWriter = new IODataWriter();
  }

  public void Open() throws IOException {
    Open(this.serverip, this.serverport);
  }

  public void Open(String ip, int port)  {
      try {
          this.serverip = ip;
          this.serverport = port;
          InetSocketAddress addr = new InetSocketAddress(ip, port);
          this.socket.connect(addr, 5000);
          
          setIODataReader(this.socket.getInputStream());
          setIODataWriter(this.socket.getOutputStream());
      } catch (IOException ex) {
          LogBackLogger.getInstance().getExceptionLogger().debug("CONNECTION ERRER : " +ex.getMessage());
//          Logger.getLogger(TCPClientIO.class.getName()).log(Level.SEVERE, null, ex);
      }
  }

  public void close() throws IOException {
    this.ioDataReader.close();
    this.ioDataWriter.close();

    this.socket.close();
  }

  private void setIODataReader(InputStream in) {
    this.ioDataReader.setInputStream(in);
  }

  private void setIODataWriter(OutputStream out) {
    this.ioDataWriter.setOutputStream(out);
  }

  public class IODataWriter
  {
    private DataOutputStream dataOut;
    private OutputStream _out;
    private byte[] lastTx = null;

    public IODataWriter() {  }

    public void close() throws IOException { 
        if (this.dataOut != null)
            this.dataOut.close(); 
    }

    public byte[] getLastTXData()
    {
      return this.lastTx;
    }

    public void setOutputStream(OutputStream out) {
      this._out = out;
      this.dataOut = new DataOutputStream(this._out);
    }

    public void setTXDatas(byte[] txbuffer) throws IOException {
      this.lastTx = ((byte[])txbuffer.clone());
      
//      System.out.println("     ----- TCP Socket Write Data -----");
//      printBytes(lastTx);
      if (this.dataOut != null) {
        this.dataOut.write(txbuffer);
      }
    }
  }

  public class IODataReader
  {
    private DataInputStream dataIn;
    private InputStream _in;
    private byte[] lastRx = null;

    public IODataReader() {  }

    public void close() throws IOException { 
        if (this.dataIn != null)
            this.dataIn.close(); 
    }

    public byte[] getLastRXData()
    {
      return this.lastRx;
    }

    public void setInputStream(InputStream in) {
      this._in = in;
      this.dataIn = new DataInputStream(this._in);
    }

    public int getRXDatas() throws IOException {
        int len = 0;
        if (this.dataIn != null) {
            len = this.dataIn.available();
            if (len > 0) {
                byte[] tmp = new byte[len];
                this.dataIn.read(tmp);

    //            System.out.println("     ----- TCP Socket Read Data -----");
    //            printBytes(tmp);

                this.lastRx = ((byte[])tmp.clone());
            }
        }
        return len;
    }
  }
  
  private void printBytes(byte[] datas) {
    String str = "";
    int i = 0; for (int n = datas.length; i < n; i++) {
      str = str + String.format("0x%02X ", new Object[] { datas[i]});
    }
    System.out.println(str);
  }
}