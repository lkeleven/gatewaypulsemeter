/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socketiomodule;

import java.util.ArrayList;

/**
 *
 * @author ccm
 */
public class GEMSTCPMessages {
    private final ArrayList<GEMSTCPMessage> arrayList;
    public GEMSTCPMessages() {
        arrayList = new ArrayList<>();
    }

    public int getDataCount() {
        return arrayList.size();
    }
    
    public GEMSTCPMessage getGEMSTCPMessage(int index) {
        GEMSTCPMessage tcpMsg = null;
        if (arrayList.size() > index) {
            tcpMsg = arrayList.get(index);
        }   
        return tcpMsg;
    }
    public GEMSTCPMessage getFirstMessage() {
        return arrayList.get(0);
    }
    public void removeGEMSTCPMessage(int index) {
        if (arrayList.size() > index)
            arrayList.remove(index);
    }
    public void removeFirstMessage() {
        arrayList.remove(0);
    }
    
    public void addMeterRealData(GEMSTCPMessageType type, byte[] data) {
        GEMSTCPMessage tcpMsg = new GEMSTCPMessage(type,data);
        arrayList.add(tcpMsg);
    }

    public void addMGEMSAuthData(byte[] data) {
        GEMSTCPMessage tcpMsg = new GEMSTCPMessage(GEMSTCPMessageType.gmtAuth, data);
        arrayList.add(0,tcpMsg);
    }
    
    public void listClear() {
        arrayList.clear();
    }   
    
    public class GEMSTCPMessage {
        private GEMSTCPMessageType gmt;
        public void setGEMSTCPMEssageType(GEMSTCPMessageType type) {
            gmt = type;
        }
        public GEMSTCPMessageType getGEMSTCPMessageType() {
            return gmt;
        }
        private byte[] gmtData;
        public void setGEMSTCPMessageData(byte[] data) {
            gmtData = data.clone();
        }
        public byte[] getGEMSTCPMessageData() {
            return gmtData;
        }
        public GEMSTCPMessage(GEMSTCPMessageType type, byte[] data) {
            gmt = type;
            gmtData = data.clone();
                    
        }
    }
    
    //getAuthDataMessage putStatusDataMessage putMeterDataMessage 
    //getTimeMeterDataMessage getHourMeterDataMessage 
    //setDurationDataMessage setCountDataMessage setCalculateDataMessage setTimeOffsetDataMessage
    public enum GEMSTCPMessageType {gmtAuth, gmtStatus, gmtMeter, gmtTimeMeter, gmtHourMeter,
                                    gmtDuration, gmtCount, gmtCalc, gmtTimeOffset};
    
    
    
}
