/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socketiomodule;

/**
 *
 * @author ccm
 */
public interface GEMSTCPClientListener {
    public void onSetDurationDataRequest(short duration);
    public void onSetCountDataRequest(byte count);
    public void onSetCalculateDataRequest(float pct, short constnum);
    public void onSetTimeOffsetDataRequest(byte offset);
}
