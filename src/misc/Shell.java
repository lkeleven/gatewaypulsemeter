package misc;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import logback.LogBackLogger;

public class Shell
{
  public void reboot()
    throws Exception
  {
    String cmd = "sudo reboot";
    shellCmd(cmd);
  }

  public String[] listSSID() throws Exception {
    String cmd = "sudo iwlist ra0 scanning";
    List dataList = shellCmd(cmd);
    List ssidList = new ArrayList();

    for (int i = 0; i < dataList.size(); i++) {
      String line = (String)dataList.get(i);
      if (line.contains("ESSID:")) {
        line = line.replace("ESSID:", "");
        line = line.replace("\"", "");
        line = line.trim();
        ssidList.add(line);
      }
    }

    String[] SSIDList = new String[ssidList.size()];

    for (int i = 0; i < ssidList.size(); i++) {
      SSIDList[i] = ((String)ssidList.get(i));
    }
    return SSIDList;
  }

  private List<String> shellCmd(String command)
    throws Exception
  {
    Runtime runtime = Runtime.getRuntime();
    Process process = runtime.exec(command);
    InputStream is = process.getInputStream();
    InputStreamReader isr = new InputStreamReader(is);
    BufferedReader br = new BufferedReader(isr);

    List dataList = new ArrayList();
    String line;
    while ((line = br.readLine()) != null)
    {
      dataList.add(line);
    }
    return dataList;
  }

  private List<String> shellCmd(String[] command) throws Exception {
    Runtime runtime = Runtime.getRuntime();
    Process process = runtime.exec(command);
    InputStream is = process.getInputStream();
    InputStreamReader isr = new InputStreamReader(is);
    BufferedReader br = new BufferedReader(isr);

    List dataList = new ArrayList();
    String line;
    while ((line = br.readLine()) != null)
    {
      System.out.println(line);
      dataList.add(line);
    }
    return dataList;
  }

  private static void shellScript(String[] script) {
    try {
      Process ps = Runtime.getRuntime().exec(script);
      ps.waitFor();

      ps.destroy();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void enableZigbeePort() throws Exception {
    String command = "echo BB-UART1 > /sys/devices/bone_capemgr.9/slots";
    shellCmd(command);
  }

  public void connectAccessPointWPA(String ssid, String password)
    throws Exception
  {
    String command = "ifconfig ra0 up";
    shellCmd(command);

    command = String.format("wpa_passphrase %s %s > /etc/wpa_supplicant/wpa_supplicant.conf", new Object[] { ssid, password });
    shellCmd(command);
    command = String.format("iwconfig ra0 essid %s", new Object[] { ssid });
    shellCmd(command);
    command = "nohup wpa_supplicant -ira0 -c/etc/wpa_supplicant/wpa_supplicant.conf &";
    BackgroundProcess bp = new BackgroundProcess(command);
    bp.start();
  }

  public void connectAccessPointWEP(String ssid, String password)
    throws Exception
  {
    String command = "ifconfig ra0 up";
    shellCmd(command);

    command = String.format("iwconfig ra0 essid %s", new Object[] { ssid });
    shellCmd(command);

    if (!"".equals(password.trim())) {
      command = String.format("iwconfig ra0 key %s", new Object[] { password });
      shellCmd(command);
      command = "iwconfig ra0 enc on";
      shellCmd(command);
    }
  }

  public void setStaticEthernet(String device, String ipv4, String subnet, String gateway, String nameserver) throws Exception
  {
    String command = String.format("ifconfig %s up", new Object[] { device });
    shellCmd(command);
    command = String.format("ifconfig %s %s netmask %s", new Object[] { device, ipv4, subnet });
    shellCmd(command);

    if (!"".equals(gateway.trim())) {
      command = String.format("route add default gw %s %s", new Object[] { gateway, device });
      shellCmd(command);
    }

    if (!"".equals(nameserver.trim())) {
      command = String.format("echo nameserver %s > /etc/resolv.conf", new Object[] { nameserver });
      shellCmd(command);
    }
  }

  public void setDHCPEthernet(String device) throws Exception
  {
    String command = String.format("ifconfig %s up", new Object[] { device });
    shellCmd(command);
    command = String.format("bash /sbin/dhcpcd %s", new Object[] { device });
    shellCmd(command);
  }

  public void setIOInitialize() throws Exception
  {
    String command = "i2cset -y 1 0x20 0x00 0xC0";
    shellCmd(command);
    command = "i2cset -y 1 0x20 0x01 0x00";
    shellCmd(command);
  }

  public int getIOBank0()
    throws Exception
  {
    String command = "i2cget -y 1 0x20 0x12";
    List dataList = shellCmd(command);
    int tmp = Integer.decode((String)dataList.get(0)).intValue();
    return tmp;
  }

  public int getIOBank1()
    throws Exception
  {
    String command = "i2cget -y 1 0x20 0x13";
    List dataList = shellCmd(command);
    int tmp = Integer.decode((String)dataList.get(0)).intValue();
    return tmp;
  }

  public void setIOBank0(int value) throws Exception
  {
    String command = String.format("i2cset -y 1 0x20 0x12 0x%s", new Object[] { Integer.toHexString(value) });
    shellCmd(command);
  }

  public void setIOBank1(int value) throws Exception
  {
    String command = String.format("i2cset -y 1 0x20 0x13 0x%s", new Object[] { Integer.toHexString(value) });
    shellCmd(command);
  }

  public void setDateTime(Date date, int offset) throws Exception {
      Calendar cal = Calendar.getInstance(); 
      cal.setTime(date);
      cal.add(Calendar.MINUTE, offset);
      
      SimpleDateFormat df = new SimpleDateFormat("MMddHHmmyyyy.ss");
      String command = String.format("sudo date %s", df.format(cal.getTime()));
      List<String> ln = shellCmd(command);
      ln.size();
  }
  
  public void setDateTimeSync(Date date) throws Exception {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);

    int min = cal.get(Calendar.MINUTE);
    int sec = cal.get(Calendar.SECOND);

    int total = (min * 60) + sec;

    int zero = 3600;
    if (total < 180) {  // 3분
        zero = -total;
    } else if (total > 3420) { // 57분
        zero = (60*60) - total;
    }
    int _15min = (15*60) - total; 
    int _30min = (30*60) - total;
    int _45min = (45*60) - total;
    int gap = 60*3;

    if ( (zero <= gap) & (zero >= -gap) ) {
        cal.add(Calendar.SECOND, zero);
    }
    if ( (_15min <= gap) & (_15min >= -gap) ) {
        cal.add(Calendar.SECOND, _15min);
    }
    if ( (_30min <= gap ) & (_30min >= -gap) ) {
        cal.add(Calendar.SECOND, _30min);
    }
    if ( (_45min <= gap ) & (_45min >= -gap) ){
        cal.add(Calendar.SECOND, _45min);
    }
    
    SimpleDateFormat df = new SimpleDateFormat("MMddHHmmyyyy.ss");
    String command = String.format("sudo date %s", df.format(cal.getTime()));
    List<String> ln = shellCmd(command);
    ln.size();
    
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    //String re = "EOI TIME : " + sdf.format(new Date()) + " - " + sdf.format(cal.getTime());
//    System.out.println( re );
    //LogBackLogger.getInstance().getInfoLogger().debug(re);
  }
  
  private class BackgroundProcess extends Thread
  {
    String commandToRun;

    public BackgroundProcess(String command)
    {
      this.commandToRun = command;
    }

    public void run() {
      try {
        Shell.this.shellCmd(this.commandToRun);
      } catch (Exception ex) {
          LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//        Logger.getLogger(Shell.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
}