/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ccm
 */
public class LogBackLogger {
    private static final String LOGBACK_XML_PATH = "/etc/gateway/logback.xml";
    
    private static LogBackLogger instance = null;
    public static LogBackLogger getInstance() {
        if (instance == null) {
            instance = new LogBackLogger();
            instance.setLogBackXML(LOGBACK_XML_PATH);
        }
        return instance;
    }
    
    public void setLogBackXML(String path) {
        System.setProperty("logback.configurationFile", path);
    }
    
    public Logger getLogger(String name) {
        return LoggerFactory.getLogger(name);
    }
    public Logger getLogger(Object obj) {
        return LoggerFactory.getLogger(obj.getClass());
    }
    
    public Logger getExceptionLogger() {
        return LoggerFactory.getLogger("exception");
    }
    public Logger getPacketLogger() {
        return LoggerFactory.getLogger("packet");
    }
    public Logger getInfoLogger() {
        return LoggerFactory.getLogger("info");
    }
    
//    public Logger getDataLogger() {
//        return LoggerFactory.getLogger("data");
//    }
    
    public Logger getSTDOUTLogger() {
        return LoggerFactory.getLogger("stdout");
    }
}
