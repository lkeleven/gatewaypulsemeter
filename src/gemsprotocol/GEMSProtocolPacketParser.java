/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gemsprotocol;

import java.util.Arrays;

/**
 *
 * @author ccm
 */
public class GEMSProtocolPacketParser {
    private byte msgType;
    public byte getMsgType() {
        return msgType;
    }
    
    private String protocolVer;
    public String getProtocolVer() {
        return protocolVer;
    }
    
    private byte[] serviceKey;
    public byte[] getServiceKey() {
        return serviceKey;
    }

    private String mobileNum;
    public String getMobileNum() {
        return mobileNum;
    }
    
    private byte[] payloadLen;
    public byte[] getPayloadLen() {
        return payloadLen;
    }
    
    private byte encrypted;
    public byte getEncrypted() {
        return encrypted;
    }
    
    private byte[] crc16;
    public byte[] getCrc16() {
        return crc16;
    }
    
    private byte[] encryptedKey;    
    public void setEncryptedKey(byte[] key) {
        encryptedKey = key;
    }
    public byte[] getEncryptedKey() {
        return encryptedKey;
    }
    
    private byte[] payload;
    public byte[] getPayload() {
        return payload;
    }
    
    public byte[] setGEMSTCPPacket(byte[] data) {
        msgType = parserMsgType(data);
        byte protocolver = parserProtocolVer(data);
        protocolVer = byteToProtocolVer(protocolver);
        serviceKey = parserServiceKey(data);
        mobileNum = new String(parserMobileNo(data));
        payloadLen = parserPayloadLen(data);
        encrypted = parserEncrypted(data);
        crc16 = parserCrc16(data);
        
        payload = new byte[0];
        
        byte[] pyld = parserPayload(data);
        byte[] payloadCrc16 = GEMSUtils.gen_crc16_bytes(pyld, pyld.length);
        
        if (Arrays.equals(crc16, payloadCrc16)) {
            byte[] decrypted;
            if (encrypted == 1) {
                 decrypted = GEMSUtils.aesDecryptEcb(new String(encryptedKey), pyld);
            } else {
                decrypted = pyld.clone();
            }
            
            int secLen = 0;
            for (int i=0; i<decrypted.length; i++) {
                if (decrypted[i] == 0x23) {
                    secLen = i;
                    break;
                }
            }
            if (secLen == 0) {
                payload = decrypted.clone();
            } else {
                payload = Arrays.copyOf(decrypted, secLen);
            }
        }
        return payload;
    }
    
    
    private byte parserMsgType(byte[] data) {
        return data[0];
    }
    private byte parserProtocolVer(byte[] data) {
        return data[1];
    }
    private byte[] parserServiceKey(byte[] data) {
        byte[] skey = new byte[32];
        System.arraycopy(data, 2, skey, 0, skey.length);
        return skey.clone();
    }
    private byte[] parserMobileNo(byte[] data) {
        byte[] mnum = new byte[11];
        System.arraycopy(data, 34, mnum, 0, mnum.length);
        return mnum.clone();
    }
    private byte[] parserPayloadLen(byte[] data) {
        byte[] len = new byte[2];
        System.arraycopy(data, 45, len, 0, len.length);
        return len.clone();
    }
    private byte parserEncrypted(byte[] data) {
        return data[47];
    }
    private byte[] parserCrc16(byte[] data) {
        byte[] crc = new byte[2];
        System.arraycopy(data, 48, crc, 0, crc.length);
        return crc.clone();
    }
    private byte[] parserPayload(byte[] data) {
        byte[] payld = new byte[data.length - 50];
        System.arraycopy(data, 50, payld, 0, payld.length);
        return payld.clone();
    }
    
    private String byteToProtocolVer(byte ver) {
        return String.valueOf(( (ver & 0xF0) >> 4) ) + "." + String.valueOf(ver & 0x0F);
    }
   
}
