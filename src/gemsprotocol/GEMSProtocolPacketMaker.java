/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gemsprotocol;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author ccm
 */
public class GEMSProtocolPacketMaker {
    
    public GEMSProtocolPacketMaker() {
        serviceKey = new byte[32];
        Arrays.fill(serviceKey, (byte)0x00);
        encryptedKey = new byte[0];
        Arrays.fill(encryptedKey, (byte)0x00);
    }
    
    private byte[] serviceKey;
    public void setServiceKey(byte[] key) {
        serviceKey = key;
    }
    public byte[] getServiceKey() {
        return serviceKey;
    }

    private byte[] encryptedKey;    
    public void setEncryptedKey(byte[] key) {
        encryptedKey = key;
    }
    public byte[] getEncryptedKey() {
        return encryptedKey;
    }

    private String protocolVer;
    public void setProtocolVer(String ver) {
        protocolVer = ver;
    }
    public String getProtocolVer() {
        return protocolVer;
    }

    private String mobileNum;
    public void setMobileNum(String num) {
        mobileNum = num;                 
    }
    public String getMobileNum() {
        return mobileNum;
    }

    private byte encrypted;
    public void setEncrypted(byte encry) {
        encrypted = encry;                    
    }
    public byte getEncrypted() {
        return encrypted;
    }

    public byte[] getGEMSTCPPacket(byte msgType, byte[] payload) throws IOException {
        ByteArrayOutputStream gemsArray = new ByteArrayOutputStream();

        // MSG TYPE
        gemsArray.write(msgType);

        // PROTOCOL VER
        byte ver = protocolToByte(protocolVer);
        gemsArray.write(ver);        

        // SERVICE KEY 
        gemsArray.write(serviceKey);

        // MOBILE
        gemsArray.write(mobileNum.getBytes());

        byte[] encryptedData = new byte[0];
        if (payload != null) {
            if ( (payload.length >0) & (encrypted == 1) ){
                int div = (payload.length / 16);
                int mod = (payload.length % 16);
                int len = div * 16;
                if (mod > 0) {
                    len += 16;
                }

                encryptedData = new byte[len];
                Arrays.fill(encryptedData, (byte)0x23);
                System.arraycopy(payload, 0, encryptedData, 0, payload.length);

                encryptedData = GEMSUtils.aesEncryptEcb(new String(encryptedKey), encryptedData);
            } else {
                encryptedData = payload.clone();
            }
        }
        
        // PAYLOAD LEN
        gemsArray.write(GEMSUtils.shortToByteArray((short) (encryptedData.length & 0xFFFF)));

        // ENCRYPTED
        gemsArray.write(encrypted); 

        // CRC16
        byte[] crc16 = GEMSUtils.gen_crc16_bytes(encryptedData, encryptedData.length);
        gemsArray.write(crc16);

        // PAYLOAD = CUSTOMER ID
        gemsArray.write(encryptedData);

        return gemsArray.toByteArray();
    }

    private byte protocolToByte(String ver) {
        int p = ver.indexOf('.');
        String mj = ver.substring(0, p);
        String mv = ver.substring(p+1,ver.length());

        return (byte)((Byte.valueOf(mj) << 4) + (Byte.valueOf(mv)));
    }
}
