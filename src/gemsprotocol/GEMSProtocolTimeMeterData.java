/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gemsprotocol;

import gemsprotocol.GEMSProtocolTypes.GEMSResponseType;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdbcmodules.JDBCPulseMeterSelect;
import logback.LogBackLogger;
import meterdatamodels.MeterTrendDatas;
import meterdatamodels.MeterTrendDatas.MeterTrendData;

/**
 *
 * @author ccm
 */
public class GEMSProtocolTimeMeterData implements GEMSProtocolResponser {
    private GEMSProtocolPacketMaker gemsHeader;
    @Override
    public void setGEMSProtocolHeader(GEMSProtocolPacketMaker header) {
        gemsHeader = header;
    }
    
    private JDBCPulseMeterSelect meterSelect;
    public void setJDBCPulseMeterSelect(JDBCPulseMeterSelect select) {
        meterSelect = select;
    }
    
    private int gemsDuration;
    public void setGEMSDuration(int duration) {
        gemsDuration = duration;
    }
    public int getGEMSDuration() {
        return gemsDuration;                
    }
    
    public byte[] getTimeMeterDataRequest(byte[] data,  float pulsePCT, int pulseConst) throws IOException {
        ByteArrayOutputStream gemsArray = new ByteArrayOutputStream();
     
        GEMSProtocolPacketParser parser = new GEMSProtocolPacketParser();
        parser.setEncryptedKey(gemsHeader.getEncryptedKey());
        parser.setGEMSTCPPacket(data); 
        
        byte[] pyld = parser.getPayload();

        Date dt = dataToDateTime(pyld);
        
        gemsArray.write(GEMSUtils.floatToByteArray(pulsePCT));
        gemsArray.write(GEMSUtils.shortToByteArray((short)pulseConst));
        gemsArray.write(GEMSUtils.shortToByteArray((short)gemsDuration));
        gemsArray.write(getTimeMeterDatas(dt, pulsePCT, pulseConst));

        gemsHeader.setEncrypted((byte)1);
        return gemsHeader.getGEMSTCPPacket((byte)0x51, gemsArray.toByteArray());
    }
    
    private Date dataToDateTime(byte[] data) {
        String sDate = new String(Arrays.copyOf(data, 8));
        String sTime = new String(Arrays.copyOfRange(data, 8, data.length));
        
        SimpleDateFormat simDF = new SimpleDateFormat("yyyyMMdd HHmm");
        Date dt = null;
        try {
            dt = simDF.parse(sDate + " " + sTime);
        } catch (ParseException ex) {
            LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//            Logger.getLogger(GEMSProtocolHourMeterData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dt;
    }
    
    private byte[] getTimeMeterDatas(Date dt, float pulsePCT, int pulseConst) throws IOException {
        ByteArrayOutputStream gemsArray = new ByteArrayOutputStream();
        
        try {
            ResultSet rs = meterSelect.selectPulseMeterTrend("TBLMINPULSE", dt);
            
            MeterTrendDatas datas = new MeterTrendDatas();
            while (rs.next()) {
                Timestamp ts = rs.getTimestamp("SAVETIME");
                Date date = new Date(ts.getTime());
                int pulseCount = rs.getInt("PULSE");
                datas.addMeterRealData(pulseCount, date);
            }
            
            if (datas.getDataCount() > 0) {
                MeterTrendData data = datas.getMeterRealData(0);
            
                gemsArray.write(DateTimeToDateString(data.getPulsedate()).getBytes());
                gemsArray.write(DateTimeToTimeString(data.getPulsedate()).getBytes());
                
                int pulseCount = data.getPulseCount();
                gemsArray.write(GEMSUtils.intToByteArray(pulseCount));
                String fvalue = String.format("%.2f", (pulseCount * pulsePCT / pulseConst) );
                gemsArray.write(GEMSUtils.floatToByteArray( Float.valueOf(fvalue) ));
            }
        } catch (SQLException ex) {
            LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//            Logger.getLogger(GEMSProtocolHourMeterData.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return gemsArray.toByteArray();
    }
    
    private String DateTimeToDateString(Date dt) {
        SimpleDateFormat sim = new SimpleDateFormat("yyyyMMdd");
        return sim.format(dt);
    }
    private String DateTimeToTimeString(Date dt) {
        SimpleDateFormat sim = new SimpleDateFormat("HHmm");
        return sim.format(dt);
    }
    
    
    public GEMSResponseType getTimeMeterDataResponse(byte[] data) {
        GEMSProtocolPacketParser parser = new GEMSProtocolPacketParser();
        parser.setEncryptedKey(gemsHeader.getEncryptedKey());
        parser.setGEMSTCPPacket(data);        
        
        return GEMSProtocolTypes.getGEMSResponseType(parser.getMsgType());
    }

    @Override
    public GEMSResponseType setGEMSTCPProtocolResponse(byte[] data) {
        return getTimeMeterDataResponse(data);
    }
    
    
}
