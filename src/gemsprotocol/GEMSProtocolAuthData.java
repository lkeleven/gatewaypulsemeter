/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gemsprotocol;

import gemsprotocol.GEMSProtocolTypes.GEMSResponseType;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import logback.LogBackLogger;

/**
 *
 * @author ccm
 */
public class GEMSProtocolAuthData implements GEMSProtocolResponser{
    private String customerId;
    public void setCustomerId(String id) {
        customerId = id;                 
    }
    public String getCustomerId() {
        return customerId;
    }
    
    private GEMSProtocolPacketMaker gemsHeader;
    @Override
    public void setGEMSProtocolHeader(GEMSProtocolPacketMaker header) {
        gemsHeader = header;
    }
    
    private ProtocolAuthDataListener authDataListener = null;
    public void setAuthDataListener(ProtocolAuthDataListener authListener) {
        authDataListener = authListener;
    }
    
    public byte[] getAuthInfoRequest() throws IOException {
        byte[] skey = new byte[32];
        Arrays.fill(skey, (byte)0);
        
        // 최초 ServiceKey는 모두 0 처리 : 암호화 없이 진행
        gemsHeader.setServiceKey(skey);
        gemsHeader.setEncrypted((byte)0);
        
        // Auth Payload 는 Customer ID
        byte[] data = gemsHeader.getGEMSTCPPacket((byte)0x03, customerId.getBytes());
        
        return data; 
    }
    
    public void getAuthInfoResponse(byte[] data) throws UnsupportedEncodingException {
        byte msgtype = parserMsgType(data);
        byte[] servicekey = parserServiceKey(data);
        byte[] crc16 = parserCrc16(data);
        
        byte[] payload = parserPayload(data);
        
        byte[] payloadCrc16 = GEMSUtils.gen_crc16_bytes(payload, payload.length);
        if ( (msgtype == 0x13) & (Arrays.equals(crc16, payloadCrc16))) {
            int secLen = 0;
            for (int i=0; i<payload.length; i++) {
                if (payload[i] == 0x23) {
                    secLen = i;
                    break;
                }
            }

            if (authDataListener != null) 
                authDataListener.onGetAuthDataRequest(servicekey, Arrays.copyOf(payload, secLen));
        }
        
    }
    
    private byte parserMsgType(byte[] data) {
        return data[0];
    }
    private byte[] parserServiceKey(byte[] data) {
        byte[] skey = new byte[32];
        System.arraycopy(data, 2, skey, 0, skey.length);
        return skey.clone();
    }
    private byte[] parserCrc16(byte[] data) {
        byte[] crc = new byte[2];
        System.arraycopy(data, 48, crc, 0, crc.length);
        return crc.clone();
    }
    private byte[] parserPayload(byte[] data) {
        byte[] payld = new byte[data.length - 50];
        System.arraycopy(data, 50, payld, 0, payld.length);
        return payld.clone();
    }
    
    @Override
    public GEMSResponseType setGEMSTCPProtocolResponse(byte[] data) {
        try {
            getAuthInfoResponse(data);
            
            return GEMSResponseType.grtAck;
        } catch (UnsupportedEncodingException ex) {
            LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//            Logger.getLogger(GEMSProtocolAuthData.class.getName()).log(Level.SEVERE, null, ex);
            return GEMSResponseType.grtNotAuth;
        }
    }
    
    public interface ProtocolAuthDataListener {
        public void onGetAuthDataRequest(byte[] serviceKey, byte[] encrypedKey);
    }
    
}
