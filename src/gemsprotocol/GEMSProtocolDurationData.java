/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gemsprotocol;

import gemsprotocol.GEMSProtocolTypes.GEMSResponseType;
import java.io.IOException;

/**
 *
 * @author ccm
 */
public class GEMSProtocolDurationData implements GEMSProtocolResponser {
    private GEMSProtocolPacketMaker gemsHeader;
    @Override
    public void setGEMSProtocolHeader(GEMSProtocolPacketMaker header) {
        gemsHeader = header;
    }
    
    private ProtocolDurationDataListener durationDataListener = null;
    public void setDurationDataListener(ProtocolDurationDataListener durationListener) {
        durationDataListener = durationListener;
    }
    
    public byte[] setDurationDataRequest(byte[] data) throws IOException {
        GEMSProtocolPacketParser parser = new GEMSProtocolPacketParser();
        parser.setEncryptedKey(gemsHeader.getEncryptedKey());
        parser.setGEMSTCPPacket(data);     
        
        byte[] pyld = parser.getPayload();
        
        short duration = GEMSUtils.byteArrayToShort(pyld);
        if (durationDataListener != null) 
            durationDataListener.onSetDurationDataRequest(duration);
        
        gemsHeader.setEncrypted((byte)1);
        return gemsHeader.getGEMSTCPPacket((byte)0xf0, null);
        
    }
    
    public GEMSResponseType setDurationDataResponse() {
        return GEMSResponseType.grtAck;
    }
    
    @Override
    public GEMSResponseType setGEMSTCPProtocolResponse(byte[] data) {
        return setDurationDataResponse();
    }
    
    public interface ProtocolDurationDataListener {
        public void onSetDurationDataRequest(short duration);
    }
    
}
