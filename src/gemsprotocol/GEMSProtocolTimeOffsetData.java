/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gemsprotocol;

import gemsprotocol.GEMSProtocolTypes.GEMSResponseType;
import java.io.IOException;

/**
 *
 * @author ccm
 */
public class GEMSProtocolTimeOffsetData implements GEMSProtocolResponser {
private GEMSProtocolPacketMaker gemsHeader;
    @Override
    public void setGEMSProtocolHeader(GEMSProtocolPacketMaker header) {
        gemsHeader = header;
    }
    
    private ProtocolTimeOffsetDataListener timeoffsetDataListener = null;
    public void setCountDataListener(ProtocolTimeOffsetDataListener timeOffsetListener) {
        timeoffsetDataListener = timeOffsetListener;
    }
    
    public byte[] setTimeOffsetDataRequest(byte[] data) throws IOException {
        GEMSProtocolPacketParser parser = new GEMSProtocolPacketParser();
        parser.setEncryptedKey(gemsHeader.getEncryptedKey());
        parser.setGEMSTCPPacket(data);     
        
        byte[] pyld = parser.getPayload();
        
        byte offset = pyld[0];
        
        if (timeoffsetDataListener != null) 
            timeoffsetDataListener.onSetTimeOffsetDataRequest(offset);
        
        gemsHeader.setEncrypted((byte)1);
        return gemsHeader.getGEMSTCPPacket((byte)0xf0, null);
        
    }
    
    public GEMSResponseType setTimeOffsetDataResponse() {
        return GEMSResponseType.grtAck;
    }
    
    @Override
    public GEMSResponseType setGEMSTCPProtocolResponse(byte[] data) {
        return setTimeOffsetDataResponse();
    }

    public interface ProtocolTimeOffsetDataListener {
        public void onSetTimeOffsetDataRequest(byte offset);
    }       
}
