/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gemsprotocol;

import gemsprotocol.GEMSProtocolTypes.GEMSResponseType;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author ccm
 */
public class GEMSProtocolCalculateData implements GEMSProtocolResponser {
    private GEMSProtocolPacketMaker gemsHeader;
    @Override
    public void setGEMSProtocolHeader(GEMSProtocolPacketMaker header) {
        gemsHeader = header;
    }
    
    private ProtocolCalculateDataListener calculateDataListener = null;
    public void setCountDataListener(ProtocolCalculateDataListener calculateListener) {
        calculateDataListener = calculateListener;
    }
    
    public byte[] setCalculateDataRequest(byte[] data) throws IOException {
        GEMSProtocolPacketParser parser = new GEMSProtocolPacketParser();
        parser.setEncryptedKey(gemsHeader.getEncryptedKey());
        parser.setGEMSTCPPacket(data);     
        
        byte[] pyld = parser.getPayload();
        
        float pct = GEMSUtils.byteArrayToFloat( Arrays.copyOfRange(pyld, 0, 4) );
        short constnum = GEMSUtils.byteArrayToShort(Arrays.copyOfRange(pyld, 4, 6) );
        if (calculateDataListener != null) 
            calculateDataListener.onSetCalculateDataRequest(pct, constnum);
        
        gemsHeader.setEncrypted((byte)1);
        return gemsHeader.getGEMSTCPPacket((byte)0xf0, null);
    }
    
    public GEMSResponseType setDurationDataResponse() {
        return GEMSResponseType.grtAck;
    }
    
    @Override
    public GEMSResponseType setGEMSTCPProtocolResponse(byte[] data) {
        return setDurationDataResponse();
    }
    
    public interface ProtocolCalculateDataListener {
        public void onSetCalculateDataRequest(float pct, short constnum);
    }    
}
