/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gemsprotocol;

import gemsprotocol.GEMSProtocolTypes.GEMSResponseType;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdbcmodules.JDBCPulseMeterSelect;
import logback.LogBackLogger;
import meterdatamodels.MeterTrendDatas;
import meterdatamodels.MeterTrendDatas.MeterTrendData;

/**
 *
 * @author ccm
 */
public class GEMSProtocolHourMeterData implements GEMSProtocolResponser {
    private GEMSProtocolPacketMaker gemsHeader;
    @Override
    public void setGEMSProtocolHeader(GEMSProtocolPacketMaker header) {
        gemsHeader = header;
    }
    
    private JDBCPulseMeterSelect meterSelect;
    public void setJDBCPulseMeterSelect(JDBCPulseMeterSelect select) {
        meterSelect = select;
    }
    
    private int gemsDuration;
    public void setGEMSDuration(int duration) {
        gemsDuration = duration;
    }
    public int getGEMSDuration() {
        return gemsDuration;                
    }
    
    public byte[] getHourMeterDataRequest(byte[] data, float pulsePCT, int pulseConst) throws IOException {
        ByteArrayOutputStream gemsArray = new ByteArrayOutputStream();
     
        GEMSProtocolPacketParser parser = new GEMSProtocolPacketParser();
        parser.setEncryptedKey(gemsHeader.getEncryptedKey());
        parser.setGEMSTCPPacket(data); 
        
        byte[] pyld = parser.getPayload();
        
        Date dt = dataToDateTime(pyld);
        
        gemsArray.write(GEMSUtils.floatToByteArray(pulsePCT));
        gemsArray.write(GEMSUtils.shortToByteArray((short)pulseConst));
        gemsArray.write(GEMSUtils.shortToByteArray((short)gemsDuration));
        gemsArray.write(getHourMeterDatas(dt,pulsePCT,pulseConst));
        
        gemsHeader.setEncrypted((byte)1);
        
        return gemsHeader.getGEMSTCPPacket((byte)0x52, gemsArray.toByteArray());
    }
    
    private Date dataToDateTime(byte[] data) {
        
        String sDate = new String(Arrays.copyOf(data, 8));
        String sTime = new String(Arrays.copyOfRange(data, 8, data.length));
        
        SimpleDateFormat simDF = new SimpleDateFormat("yyyyMMdd HHmm");
        Date dt = null;
        try {
            dt = simDF.parse(sDate + " " + sTime);
        } catch (ParseException ex) {
            LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//            Logger.getLogger(GEMSProtocolHourMeterData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dt;
    }
    
    private byte[] getHourMeterDatas(Date dt, float pulsePCT, int pulseConst) throws IOException {
        ByteArrayOutputStream gemsArray = new ByteArrayOutputStream();
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        cal.add(Calendar.HOUR, -1);
        
        MeterTrendDatas datas = new MeterTrendDatas();
        try {
            ResultSet rs = meterSelect.selectPulseMeterTrend(getTableName(), cal.getTime(), dt);

            while (rs.next()) {
                Date pulseDate = rs.getTimestamp("SAVETIME");
                int pulseCount = rs.getInt("PULSE");
                datas.addMeterRealData(pulseCount, pulseDate);
            }
            
            SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd HHmmss");
            //System.out.println(" <HOUR Meter Data Start : " + df.format(dt) + " - " + df.format(cal.getTime()));
            for (int i = 0; i < datas.getDataCount(); i++) {
                MeterTrendData data = datas.getMeterRealData(i);

                gemsArray.write(DateTimeToDateString(data.getPulsedate()).getBytes());
                gemsArray.write(DateTimeToTimeString(data.getPulsedate()).getBytes());
                int pulseCount = data.getPulseCount();
                gemsArray.write(GEMSUtils.intToByteArray(pulseCount));
                String fvalue = String.format("%.2f", (pulseCount * (double)pulsePCT / (double)pulseConst) );
                gemsArray.write(GEMSUtils.floatToByteArray( Float.valueOf(fvalue) ));
                
                //System.out.println("HOUR Meter Data : " + df.format(data.getPulsedate()) + " - " + pulseCount + " [" + fvalue + "]");
            }

        } catch (SQLException ex) {
            LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//            Logger.getLogger(GEMSProtocolHourMeterData.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println(" <HOUR Meter Data END");
        return gemsArray.toByteArray();
    }
    
    private String getTableName() {
        switch(gemsDuration) {
            case 60: return "TBLMINPULSE";
            case 300: return "TBL5MINPULSE";
            case 900: return "TBL15MINPULSE";
            default: return "TBLMINPULSE";
        }
    }
    
    private String DateTimeToDateString(Date dt) {
        SimpleDateFormat sim = new SimpleDateFormat("yyyyMMdd");
        return sim.format(dt);
    }
    private String DateTimeToTimeString(Date dt) {
        SimpleDateFormat sim = new SimpleDateFormat("HHmm");
        return sim.format(dt);
    }
    
    
    public GEMSResponseType getHourMeterDataResponse(byte[] data) {
        GEMSProtocolPacketParser parser = new GEMSProtocolPacketParser();
        parser.setEncryptedKey(gemsHeader.getEncryptedKey());
        parser.setGEMSTCPPacket(data);        
                
        return GEMSProtocolTypes.getGEMSResponseType(parser.getMsgType());
    }

    @Override
    public GEMSResponseType setGEMSTCPProtocolResponse(byte[] data) {
        return getHourMeterDataResponse(data);
    }
    
}
