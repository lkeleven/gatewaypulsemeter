/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gemsprotocol;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import gemsprotocol.GEMSProtocolTypes.GEMSResponseType;

/**
 *
 * @author ccm
 */
public class GEMSProtocolStatusData implements GEMSProtocolResponser {
    private GEMSProtocolPacketMaker gemsHeader;
    @Override
    public void setGEMSProtocolHeader(GEMSProtocolPacketMaker header) {
        gemsHeader = header;
    }
        
    public byte[] putStatusDataRequest(int event) throws IOException {
        ByteArrayOutputStream gemsArray = new ByteArrayOutputStream();
        
        Date now = new Date();
        
        String nDate = new SimpleDateFormat("yyyyMMdd").format(now);
        String nTime = new SimpleDateFormat("HHmm").format(now);
        gemsArray.write(nDate.getBytes());
        gemsArray.write(nTime.getBytes());
        gemsArray.write((byte)event);
        
        gemsHeader.setEncrypted((byte)1);
        return gemsHeader.getGEMSTCPPacket((byte)0x02, gemsArray.toByteArray());
    }
    
    public GEMSResponseType putStatusDataResponse(byte[] data) {
        GEMSProtocolPacketParser parser = new GEMSProtocolPacketParser();
        parser.setEncryptedKey(gemsHeader.getEncryptedKey());
        parser.setGEMSTCPPacket(data);        
        
        return GEMSProtocolTypes.getGEMSResponseType(parser.getMsgType());
    }

    @Override
    public GEMSResponseType setGEMSTCPProtocolResponse(byte[] data) {
        return putStatusDataResponse(data);
    }
}
