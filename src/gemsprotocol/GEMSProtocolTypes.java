/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gemsprotocol;

/**
 *
 * @author ccm
 */
public class GEMSProtocolTypes {
    public enum MeterDataType {mdtSec, mdt1Min, mdt5Min, mdt15Min};
    
    public enum GEMSResponseType {grtAck, grtAckPiggy, grtNotAuth, grtNack};
    public static GEMSResponseType getGEMSResponseType(byte type) {
        GEMSResponseType grt;
        switch (type) {
            case (byte)0xf0: grt = GEMSResponseType.grtAck; break;
            case (byte)0xf1: grt = GEMSResponseType.grtAckPiggy; break;
            case (byte)0xfe: grt = GEMSResponseType.grtNotAuth; break;
            case (byte)0xff: grt = GEMSResponseType.grtNack; break;
            default: grt = GEMSResponseType.grtAck;
        }
        return grt;
    }
    
    public enum GEMSPiggyType {gptTimeMeter, gptHourMeter, gptDuration, gptCount, gptCalculate, gptTimeOffset};
    public static GEMSPiggyType getGEMSPiggyType(byte type) {
        GEMSPiggyType gpt;
        switch (type) {
            case (byte)0x41: gpt = GEMSPiggyType.gptTimeMeter; break;
            case (byte)0x42: gpt = GEMSPiggyType.gptHourMeter; break;
            case (byte)0x43: gpt = GEMSPiggyType.gptDuration; break;
            case (byte)0x44: gpt = GEMSPiggyType.gptCount; break;
            case (byte)0x45: gpt = GEMSPiggyType.gptCalculate; break;
            case (byte)0x46: gpt = GEMSPiggyType.gptTimeOffset; break;
            default: gpt = GEMSPiggyType.gptTimeMeter; break;
        }
        return gpt;
    }
}
