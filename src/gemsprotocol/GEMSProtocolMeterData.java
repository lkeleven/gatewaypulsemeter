/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gemsprotocol;

import gemsprotocol.GEMSProtocolTypes.GEMSResponseType;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import logback.LogBackLogger;
import meterdatamodels.MeterRealDatas;
import meterdatamodels.MeterRealDatas.MeterRealData;

/**
 *
 * @author ccm
 */
public class GEMSProtocolMeterData implements GEMSProtocolResponser {
    private GEMSProtocolPacketMaker gemsHeader;
    @Override
    public void setGEMSProtocolHeader(GEMSProtocolPacketMaker header) {
        gemsHeader = header;
    }
    
    private int gemsDuration;
    public void setGEMSDuration(int duration) {
        gemsDuration = duration;
    }
    public int getGEMSDuration() {
        return gemsDuration;                
    }
    
    private int gemsCount;
    public void setGEMSCount(int count) {
        gemsCount = count;
    }
    public int getGEMSCount() {
        return gemsCount;
    }
    
    public byte[] putMeterDataRequest(MeterRealDatas mrds, float pulsePCT, int pulseConst) throws IOException {
        ByteArrayOutputStream gemsArray = new ByteArrayOutputStream();
        
        gemsArray.write(GEMSUtils.floatToByteArray((float)pulsePCT));
        gemsArray.write(GEMSUtils.shortToByteArray((short)pulseConst));
        gemsArray.write(GEMSUtils.shortToByteArray((short)gemsDuration));
        
        
        int len;
        if (mrds.getDataCount() >= gemsCount) {
            len = gemsCount;
        } else {
            len = mrds.getDataCount();
        }
        
        
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        for (int i = 0; i<mrds.getDataCount(); i++) {
            MeterRealData mrd = mrds.getMeterRealData(i);
            String fvalue = String.format("%.2f", (mrd.getPulseCount() * pulsePCT / pulseConst) );
            LogBackLogger.getInstance().getPacketLogger().debug("MeterRealData : " +  df.format(mrd.getPulsedate()) + " - " + mrd.getPulseCount() + " [" + fvalue + "] ");
        }
        
        
        for (int i = len-1; i >= 0; i--) {
            MeterRealData real = mrds.getMeterRealData(i);
            String sDate = new SimpleDateFormat("yyyyMMdd").format(real.getPulsedate());
            String sTime = new SimpleDateFormat("HHmm").format(real.getPulsedate());
            String sValue = String.format("%.2f", (double)(real.getPulseCount() * (double)pulsePCT / (double)pulseConst) );
            float fValue = Float.parseFloat(sValue);
            
            String frStr = "MeterRealData %d : %s[%s, %s] - %d - %f";
            String valueStr = String.format(frStr, i,df.format(real.getPulsedate()), sDate, sTime, real.getPulseCount(), fValue);
            LogBackLogger.getInstance().getPacketLogger().debug(valueStr);
            
            gemsArray.write(sDate.getBytes());
            gemsArray.write(sTime.getBytes());
            gemsArray.write(GEMSUtils.intToByteArray(real.getPulseCount()));            
            gemsArray.write(GEMSUtils.floatToByteArray(fValue));
        }
        
        gemsHeader.setEncrypted((byte)1);
        return gemsHeader.getGEMSTCPPacket((byte)0x01, gemsArray.toByteArray());
    }
    
    public GEMSResponseType putMeterDataResponse(byte[] data) {
        GEMSProtocolPacketParser parser = new GEMSProtocolPacketParser();
        parser.setEncryptedKey(gemsHeader.getEncryptedKey());
        parser.setGEMSTCPPacket(data);        
        
        return GEMSProtocolTypes.getGEMSResponseType(parser.getMsgType());
    }

    @Override
    public GEMSResponseType setGEMSTCPProtocolResponse(byte[] data) {
        return putMeterDataResponse(data);
    }
}
