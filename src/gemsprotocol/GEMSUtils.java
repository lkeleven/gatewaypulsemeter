/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gemsprotocol;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author ccm
 */
public class GEMSUtils {
    //--------------------- UTILS Functions ---------------------
    public static byte[] shortToByteArray(short value) {
        byte[] byteArray = new byte[2];
        byteArray[1] = (byte)(value >> 8);
        byteArray[0] = (byte)value;
        return byteArray;
    }        
         
    public static byte[] floatToByteArray(float value) {
        int floatValue = Float.floatToIntBits(value);
        return intToByteArray(floatValue);
    }

    public static byte[] intToByteArray(int value) {
        byte[] byteArray = new byte[4];
        byteArray[3] = (byte)(value >> 24);
        byteArray[2] = (byte)(value >> 16);
        byteArray[1] = (byte)(value >> 8);
        byteArray[0] = (byte)value;
        return byteArray;
    }
    
    public static short byteArrayToShort(byte bytes[]) {
        return (short)( (bytes[1] << 8 & 0xFF00) | (bytes[0] & 0xff) );
    }
    
    public static float byteArrayToFloat(byte bytes[]) {
            int value =  byteArrayToInt(bytes);
            return Float.intBitsToFloat(value);
    }
    
    public static int byteArrayToInt(byte bytes[]) {
	return ((((int)bytes[3] & 0xff) << 24) |
		(((int)bytes[2] & 0xff) << 16) |
		(((int)bytes[1] & 0xff) << 8) |
		(((int)bytes[0] & 0xff)));
    } 
    
    public static long toUnsigned(int value) {
	return ((long)value & 0xFFFFFFFFL);
    } 
    
    
    public static void printBytes(byte[] datas) {
        String str = "";
        int i = 0; for (int n = datas.length; i < n; i++) {
          str = str + String.format("0x%02X ", new Object[] { Byte.valueOf(datas[i]) });
        }
        System.out.println(str);
    }
    
    public static void printBytes(byte[] datas, int count) {
        String str = "";
        for (int i = 0; i < datas.length; i++) {
          str = str + String.format("0x%02X ", new Object[] { datas[i]});
          if (((i+1) % count) == 0) {
              System.out.println(str);
              str = "";
          }
        }
        System.out.println(str);
    }
    
    
    public static short gen_crc16(byte[] data, int size) {
    
        int CRC16 = 0x8005;

        int out = 0;
        int bits_read = 0;

        if (data == null) return 0;
        int len = 0;

        int crc = 0;

        while (len < size) {
            int bit_flag = out >> 15;
            out <<= 1;
            out |= (data[len] >> bits_read) & 0x1;

            bits_read++;
            if (bits_read > 7) {
              bits_read = 0;
              len++;
            }

            if (bit_flag == 1) {
              out ^= CRC16;
            }
          
            out &= 0xFFFF; 
        }
        

        for (int i = 0; i < 16; i++) {
            int bit_flag = out >> 15;
            out <<= 1;
            if (bit_flag == 1) {
              out ^= CRC16;
            }
            
            out &= 0xFFFF;
        }

        int ii = 0x8000;
        int j = 0x0001;
        for (; ii != 0; ii >>= 1) {
            if ((ii & out) > 0) {
              crc |= j;
            }
            j <<= 1;
        }
        return (short)(crc & 0xFFFF);
      }
 
    public static byte[] gen_crc16_bytes(byte[] data, int size) {
        int CRC16 = 0x8005;
        
        int out = 0;
        int bits_read = 0;
           
        byte[] reCrc = new byte[2];
        
        if (data == null) return reCrc;
        int len = 0;

        int crc = 0;

        while (len < size) {
            int bit_flag = out >> 15;
            out <<= 1;
            out |= (data[len] >> bits_read) & 0x1;

            bits_read++;
            if (bits_read > 7) {
              bits_read = 0;
              len++;
            }

            if (bit_flag == 1) {
              out ^= CRC16;

            }
            out &= 0xFFFF; 
        }

        for (int i = 0; i < 16; i++) {
            int bit_flag = out >> 15;
            out <<= 1;
            if (bit_flag == 1) {
              out ^= CRC16;
            }
            
            out &= 0xFFFF;
        }

        int ii = 0x8000;
        int j = 0x0001;
        for (; ii != 0; ii >>= 1) {
            if ((ii & out) > 0) {
              crc |= j;
            }
            j <<= 1;
        }

        reCrc[0] |= (byte)(crc & 0xFF);
        reCrc[1] |=  (byte)((crc & 0xFF00) >>> 8);
	return reCrc;
    }
    
    public static byte[] aesEncryptEcb(String sKey, byte[] sText) {
        byte[] key;// = null;
        byte[] text;// = null;
        byte[] encrypted;// = null;
        final int AES_KEY_SIZE_128 = 128;
 
        try {
            // UTF-8
            key = sKey.getBytes("UTF-8");
 
            // Key size 맞춤 (128bit, 16byte)
            key = Arrays.copyOf(key, AES_KEY_SIZE_128 / 8);
 
            // UTF-8
            text = sText.clone(); // sText.getBytes("UTF-8");
 
            // AES/EBC/ {PKCS5Padding(64bytes) .. NoPadding(text Size:16byte 배수)}
            Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
            cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key, "AES"));
            encrypted = cipher.doFinal(text);
            
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            encrypted = null;
        }
 
        return encrypted;
    }
    
    public static byte[] aesDecryptEcb(String sKey, byte[] encrypted) {
        byte[] key;// = null;
        byte[] decrypted;// = null;
        final int AES_KEY_SIZE_128 = 128;
 
        try {
            // UTF-8
            key = sKey.getBytes("UTF-8");
 
            // Key size 맞춤 (128bit, 16byte)
            key = Arrays.copyOf(key, AES_KEY_SIZE_128 / 8);
 
            // AES/EBC/ {PKCS5Padding(64bytes) .. NoPadding(text Size:16byte 배수)}
            Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key, "AES"));
            decrypted = cipher.doFinal(encrypted);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            decrypted = null;
        }
 
        return decrypted;
    }
       
        
    
}
