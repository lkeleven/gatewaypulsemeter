/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gemsprotocol;

import gemsprotocol.GEMSProtocolTypes.GEMSResponseType;

/**
 *
 * @author ccm
 */
public interface GEMSProtocolResponser {
    public GEMSResponseType setGEMSTCPProtocolResponse(byte[] data);
    public void setGEMSProtocolHeader(GEMSProtocolPacketMaker header);
}
