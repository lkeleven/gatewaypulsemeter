/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gemsprotocol;

import gemsprotocol.GEMSProtocolTypes.GEMSResponseType;
import java.io.IOException;

/**
 *
 * @author ccm
 */
public class GEMSProtocolCountData implements GEMSProtocolResponser {
    private GEMSProtocolPacketMaker gemsHeader;
    @Override
    public void setGEMSProtocolHeader(GEMSProtocolPacketMaker header) {
        gemsHeader = header;
    }
    
    private ProtocolCountDataListener countDataListener = null;
    public void setCountDataListener(ProtocolCountDataListener countListener) {
        countDataListener = countListener;
    }
    
    public byte[] setCountDataRequest(byte[] data) throws IOException {
        GEMSProtocolPacketParser parser = new GEMSProtocolPacketParser();
        parser.setEncryptedKey(gemsHeader.getEncryptedKey());
        parser.setGEMSTCPPacket(data);     
        
        byte[] pyld = parser.getPayload();
        
        byte count = pyld[0];
        if (countDataListener != null) 
            countDataListener.onSetCountDataRequest(count);
        
        gemsHeader.setEncrypted((byte)1);
        return gemsHeader.getGEMSTCPPacket((byte)0xf0, null);
    }
    
    public GEMSResponseType setDurationDataResponse() {
        return GEMSResponseType.grtAck;
    }
    
    @Override
    public GEMSResponseType setGEMSTCPProtocolResponse(byte[] data) {
        return setDurationDataResponse();
    }
    
    public interface ProtocolCountDataListener {
        public void onSetCountDataRequest(byte count);
    }
}
