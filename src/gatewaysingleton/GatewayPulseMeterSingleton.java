/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gatewaysingleton;

import gemsprotocol.GEMSProtocolTypes;
import gpioreader.GPIOPulseMeterListener;
import gpioreader.GPIOPulseMeterThd;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdbcmodules.JDBCDerbyConnection;
import jdbcmodules.JDBCPulseMeter;
import logback.LogBackLogger;
import meterdatamodels.MeterRealDatas;
import meterdatamodels.MeterValueData;
import misc.Shell;
import socketiomodule.GEMSTCPClient;
import socketiomodule.GEMSTCPClientListener;
import systemconfigs.IPConfig;
import systemconfigs.IniConfig;

/**
 *
 * @author ccm
 */
public class GatewayPulseMeterSingleton {
    private static GatewayPulseMeterSingleton gatewaySingleton = null;
    public static GatewayPulseMeterSingleton getGatewaySingleton() {
        if (gatewaySingleton == null) {
            gatewaySingleton = new GatewayPulseMeterSingleton();
        }
        return gatewaySingleton;
    };
    
    
//    private final String PULSE_DERBY_PATH = "//localhost:1527//etc/gateway/db/pulsemeter";
    private String pulseDerbyPath;
    public void setPulseDerbyPath(String path) {
        pulseDerbyPath = path;
    }
    public String getPulseDerbyPath() {
        return pulseDerbyPath;
    }
    
    private final JDBCDerbyConnection insertCon = new JDBCDerbyConnection();
    private final JDBCDerbyConnection selectCon = new JDBCDerbyConnection();
    private final JDBCDerbyConnection deleteCon = new JDBCDerbyConnection();
    private void createDerbyConnection() {
        
        try {
//            insertCon.dbConnection(PULSE_DERBY_PATH, null, null);
//            selectCon.dbConnection(PULSE_DERBY_PATH, null, null);
            insertCon.dbConnection(pulseDerbyPath, null, null);
            selectCon.dbConnection(pulseDerbyPath, null, null);
            deleteCon.dbConnection(pulseDerbyPath, null, null);
        } catch (ClassNotFoundException | SQLException ex) {
            LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//            Logger.getLogger(GatewayPulseMeterSingleton.class.getName()).log(Level.SEVERE, null, ex);
            try {
                Thread.sleep(3000);
            } catch (InterruptedException ex1) {
                LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//                Logger.getLogger(GatewayPulseMeterSingleton.class.getName()).log(Level.SEVERE, null, ex1);
            }
            createDerbyConnection();
        }
    }
    
    private MeterValueData _secMeterValue;
    private MeterValueData _minMeterValue;
    private MeterValueData _5minMeterValue;
    private MeterValueData _15minMeterValue;   
    private void createMeterValues() {
        
        _secMeterValue = new MeterValueData();
        _minMeterValue = new MeterValueData();
        _5minMeterValue = new MeterValueData();
        _15minMeterValue = new MeterValueData();
        
        _secMeterValue.setMeterDataType(GEMSProtocolTypes.MeterDataType.mdtSec);
        _minMeterValue.setMeterDataType(GEMSProtocolTypes.MeterDataType.mdt1Min);
        _5minMeterValue.setMeterDataType(GEMSProtocolTypes.MeterDataType.mdt5Min);
        _15minMeterValue.setMeterDataType(GEMSProtocolTypes.MeterDataType.mdt15Min);
    }
    
    private GPIOPulseMeterThd pm;
    private void createGPIOPulseMeter() {
        pm = new GPIOPulseMeterThd();
    }
    
    
    private IPConfig ipconfig;
    private void createIPConfig() {
        ipconfig = new IPConfig();
    }
    
    private IniConfig iniconfig;
    private void createIniConfig() {
        iniconfig = new IniConfig();
        iniconfig.readConfigIni_PulseMeter();
        
        pulseDerbyPath = iniconfig.getPulseDBPath();
        gemsServerIp = iniconfig.getGemsIP();
        gemsServerPort = iniconfig.getGemsPort();
        gemsProtocolVer = iniconfig.getProtocolVer();
        gemsMobileNum = iniconfig.getGemsMobile();
        gemsCustomerId = iniconfig.getGemsCustomerID();
        gemsDuration = iniconfig.getGemsDuration();
        gemsMeterRealCount = iniconfig.getGemsCount();
        gemsPulsePCT = (float)iniconfig.getPulsePCT();
        gemsPulseConst = iniconfig.getPulseConst();
    }
    
//    private final static String GEMS_SERVER_IP = "test.gridwiz.com";
//    private final static int GEMS_SERVER_PORT  = 2638;
    private String gemsServerIp;
    public void setGEMSServerIP(String ip) {
        gemsServerIp = ip;
        iniconfig.setGemsIP(gemsServerIp);
    }
    public String getGEMSServerIP() {
        return gemsServerIp;
    }
    private int gemsServerPort;
    public void setGEMSServerPort(int port) {
        gemsServerPort = port;
        iniconfig.setGemsPort(gemsServerPort);
    }
    public int getGEMSServerPort() {
        return gemsServerPort;
    }
    
//    private final static String GEMS_PROTOCOL_VER = "1.6";
//    private final static String GEMS_MOBILE_NUM = "01012345678";
    private String gemsProtocolVer;
    public void setGEMSProtocolVer(String ver) {
        gemsProtocolVer = ver;
        tcpClient.setProtocolVer(gemsProtocolVer);
        iniconfig.setProtocolVer(gemsProtocolVer);
    }
    public String getGEMSProtocolVer() {
        return gemsProtocolVer;
    }
    private String gemsMobileNum;
    public void setGEMSMobilNum(String num) {
        gemsMobileNum = num;
        tcpClient.setMobileNum(gemsMobileNum);
        iniconfig.setGemsMobile(gemsMobileNum);
    }
    public String getGEMSMobilNum() {
        return gemsMobileNum;
    }
    
//    private final static String GEMS_CUSTOMER_ID = "1";
    private String gemsCustomerId;
    public void setGEMSCustomerId(String id) {
        gemsCustomerId = id;
        tcpClient.setCustomerId(gemsCustomerId);
        iniconfig.setGemsCustomerID(gemsCustomerId);
    }
    public String getGEMSCustomerId() {
        return gemsCustomerId;
    }
    
//    private final static int GEMS_DURATION = 60;
//    private final static int GEMS_METERREAL_COUNT = 4;
    private int gemsDuration;
    public void setGEMSDuration(int value) {
        gemsDuration = value;
        tcpClient.setDuration(gemsDuration);
        iniconfig.setGemsDuration(gemsDuration);
    }
    public int getGEMSDuration() {
        return gemsDuration;
    }
    private int gemsMeterRealCount;
    public void setGEMSMeterRealCount(int count) {
        gemsMeterRealCount = count;
        tcpClient.setMeterRealCount(gemsMeterRealCount);
        iniconfig.setGemsCount(gemsMeterRealCount);
    }
    public int getGEMSMeterRealCount() {
        return gemsMeterRealCount;
    }
    
//    private final static float GEMS_PULSE_PCT = 1.0f;
//    private final static int GEMS_PULSE_CONST = 1;
    private float gemsPulsePCT;
    public void setGEMSPulsePCT(float pct) {
        gemsPulsePCT = pct;
        tcpClient.setPulsePCT(gemsPulsePCT);
        iniconfig.setPulsePCT(gemsPulsePCT);
    }
    public float getGEMSPulsePCT() {
        return gemsPulsePCT;
    }
    private int gemsPulseConst;
    public void setGEMSPulseConst(int value) {
        gemsPulseConst = value;
        tcpClient.setPulseConst(gemsPulseConst);
        iniconfig.setPulseConst(gemsPulseConst);
    }
    public int getGEMSPulseConst() {
        return gemsPulseConst;
    }
    
    
    private GEMSTCPClient tcpClient; 
    private void createGEMSTCPClient() {
        tcpClient = new GEMSTCPClient();
        
        tcpClient = new GEMSTCPClient();
        tcpClient.setDerbyConnection(selectCon);
        
        tcpClient.setGEMSServerIP(gemsServerIp);
        tcpClient.setGEMSServerPort(gemsServerPort);
        
        tcpClient.setProtocolVer(gemsProtocolVer);
        tcpClient.setMobileNum(gemsMobileNum);
        
        tcpClient.setCustomerId(gemsCustomerId);
        
        tcpClient.setDuration(gemsDuration);
        tcpClient.setMeterRealCount(gemsMeterRealCount);
        
        tcpClient.setPulsePCT(gemsPulsePCT);
        tcpClient.setPulseConst(gemsPulseConst);
        
//        try {
//            tcpClient.getAuthDataMessage();
//        } catch (IOException ex) {
//            Logger.getLogger(GatewayPulseMeterSingleton.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }
    
    
    public void GatewayPulseMeterStart() {
        createIPConfig();
        createIniConfig();
        
        createDerbyConnection();
        createMeterValues();
        createGPIOPulseMeter();
       
        createGEMSTCPClient();
        setGEMSTcpClientFunc();
        
        setGPIOPulseMeterFunc();
        pm.start();
    }
    
    private void setGPIOPulseMeterFunc() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        pm.setGPIOPulseMeterListener(new GPIOPulseMeterListener() {
            @Override
            public void onSecPulsed(Date dt, int pulseCount) {
//                System.out.println("     ----- Sec Pulse Count :: [" + dt + "] " + pulseCount);
                _secMeterValue.setPulseCount(pulseCount, dt);
                deletePulseData("TBLSECPULSE", dt);
            }

            @Override
            public void onMinPulsed(Date dt, int pulseCount) {
                LogBackLogger.getInstance().getInfoLogger().info("     ----- min Pulse Count :: [" + sdf.format(dt) + "] " + pulseCount);
                _minMeterValue.setPulseCount(pulseCount, dt);
                if (gemsDuration == 60) 
                    putMeterDataMessage();
               
                insertPulseCount("TBLMINPULSE",dt,pulseCount);
                deletePulseData("TBLMINPULSE",dt);
                //printMeterValues("MIN PULSE COUNT",_minMeterValue.getMeterRealDatas());
            }

            @Override
            public void on5MinPulsed(Date dt, int pulseCount) {
                LogBackLogger.getInstance().getInfoLogger().info("     ----- 5min Pulse Count :: [" + sdf.format(dt) + "] " + pulseCount);
                _5minMeterValue.setPulseCount(pulseCount, dt);
                if (gemsDuration == 300)
                    putMeterDataMessage();
                
                insertPulseCount("TBL5MINPULSE",dt,pulseCount);
                deletePulseData("TBL5MINPULSE",dt);
                //printMeterValues("5MIN PULSE COUNT",_5minMeterValue.getMeterRealDatas());
            }

            @Override
            public void on15MinPulsed(Date dt, int pulseCount) {
                LogBackLogger.getInstance().getInfoLogger().info("     ----- 15min Pulse Count :: [" + sdf.format(dt) + "] " + pulseCount);
                _15minMeterValue.setPulseCount(pulseCount, dt);
                if (gemsDuration == 900) 
                    putMeterDataMessage();
                
                insertPulseCount("TBL15MINPULSE",dt,pulseCount);
                deletePulseData("TBL15MINPULSE",dt);
                //printMeterValues("15MIN PULSE COUNT",_15minMeterValue.getMeterRealDatas());
            }

            @Override
            public void onMeterDoorOpen(Date dt) {
                LogBackLogger.getInstance().getInfoLogger().info("     ----- Meter Door Open [" + sdf.format(dt) + "] " );
                putStatusDataMessage();
            }

            @Override
            public void onEOIDetect(Date dt) {
                Shell shell = new Shell();
                try {
                    shell.setDateTimeSync(dt);
                    LogBackLogger.getInstance().getInfoLogger().info("     ----- EOI SIGNAL [" + sdf.format(dt) + "]  GATEWAY [" + sdf.format(new Date()) );
//                    LogBackLogger.getInstance().getSTDOUTLogger().info("     ----- EOI SIGNAL [" + sdf.format(dt) + "]  GATEWAY [" + sdf.format(new Date()) );
                } catch (Exception ex) {
                    LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//                    Logger.getLogger(GatewayPulseMeterSingleton.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        });
    }
    
    private void insertPulseCount(String table, Date dt, int pulseCount) {
        JDBCPulseMeter insert = new JDBCPulseMeter();
        insert.setConnection(insertCon.getDBConnection());
        try {
            insert.insertPulseMeterTrend(table, dt, pulseCount);
        } catch (SQLException ex) {
            LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//            Logger.getLogger(GatewayPulseMeterSingleton.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    private void deletePulseData(String table, Date dt) {
        JDBCPulseMeter deleter = new JDBCPulseMeter();
        deleter.setConnection(deleteCon.getDBConnection());
        try {
            deleter.deletePulseMeterTrend(table, dt);
        } catch (SQLException ex) {
            LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//            Logger.getLogger(GatewayPulseMeterSingleton.class.getName()).log(Level.SEVERE, null, ex);
        }
                
    }
    
    private void putMeterDataMessage() {
        
        MeterRealDatas mrds;
        switch(gemsDuration) {
            case 60: mrds = _minMeterValue.getMeterRealDatas(); break;
            case 300: mrds = _5minMeterValue.getMeterRealDatas(); break;
            case 900: mrds = _15minMeterValue.getMeterRealDatas(); break;
            default: mrds = _minMeterValue.getMeterRealDatas(); break;
        }
        
        try {
            tcpClient.putMeterDataMessage(mrds);
            //printMeterValues("GEMS TCP CLIENT :: " + String.valueOf(gemsDuration), mrds);
        } catch (IOException ex) {
            LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//            Logger.getLogger(GatewayPulseMeterSingleton.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    private void printMeterValues(String type, MeterRealDatas valuedata) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String st = "    ----- " + type + " -----";
        //LogBackLogger.getInstance().getDataLogger().debug(st);
        //LogBackLogger.getInstance().getSTDOUTLogger().debug(st);
        for (int i=0; i<valuedata.getDataCount(); i++ ) {
            MeterRealDatas.MeterRealData data = valuedata.getMeterRealData(i);
            String dataStr = String.format("%d Data : %s :: %d", i+1, sdf.format(data.getPulsedate()), data.getPulseCount() );
            //LogBackLogger.getInstance().getDataLogger().debug(dataStr);
            //LogBackLogger.getInstance().getSTDOUTLogger().debug(dataStr);
        }
        //LogBackLogger.getInstance().getDataLogger().debug("     - - - - - ");
        //LogBackLogger.getInstance().getSTDOUTLogger().debug("     - - - - - ");
    }
    
    
    
    private void putStatusDataMessage() {
        try {
            tcpClient.putStatusDataMessage(1);
        } catch (IOException ex) {
            LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//            Logger.getLogger(GatewayPulseMeterSingleton.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void setGEMSTcpClientFunc() {
        tcpClient.setGEMSTCPClientListener(new GEMSTCPClientListener() {
            @Override
            public void onSetDurationDataRequest(short duration) {
                LogBackLogger.getInstance().getInfoLogger().info("SET DURATION : " + duration);
                setGEMSDuration(duration);
                doWriteIniconfig();
            }

            @Override
            public void onSetCountDataRequest(byte count) {
                LogBackLogger.getInstance().getInfoLogger().info("SET COUNT : " + count);
                setGEMSMeterRealCount(count);
                doWriteIniconfig();
            }

            @Override
            public void onSetCalculateDataRequest(float pct, short constnum) {
                LogBackLogger.getInstance().getInfoLogger().info("SET PCT, Transt : " + pct + " , " + constnum);
                setGEMSPulsePCT(pct);
                setGEMSPulseConst(constnum);
                doWriteIniconfig();
            }

            @Override
            public void onSetTimeOffsetDataRequest(byte offset) {
                LogBackLogger.getInstance().getInfoLogger().info("SET TIMEOFFSET : " + offset);
                Shell shell = new Shell();
                try {
                    shell.setDateTime(new Date(), offset);
                } catch (Exception ex) {
                    LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//                    Logger.getLogger(GatewayPulseMeterSingleton.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
    
    private void doWriteIniconfig() {
        LogBackLogger.getInstance().getInfoLogger().info("          ===== SYSTEM CONFIG.INI WRITE =====");
        iniconfig.writeConfigIni_PulseMeter();
    }
}
