/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbcmodules;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author ccm
 */
public class JDBCPulseMeter implements JDBCPulseMeterInsert, JDBCPulseMeterSelect {

    private Connection con;
    public void setConnection(Connection dbcon) {
        con = dbcon;
    }
    
    @Override
    public int insertPulseMeterTrend(String tbl, Timestamp dt, int pulse) throws SQLException
    {
      String sql = "INSERT INTO " + tbl + " VALUES(?,?)";
      PreparedStatement pstmt = this.con.prepareStatement(sql);
      pstmt.setTimestamp(1, dt);
      pstmt.setInt(2, pulse);
      return pstmt.executeUpdate();
    }
    
    @Override
    public int insertPulseMeterTrend(String tbl, Date dt, int pulse) throws SQLException {
      String sql = "INSERT INTO " + tbl + " VALUES(?,?)";
      PreparedStatement pstmt = this.con.prepareStatement(sql);
      pstmt.setTimestamp(1, new Timestamp(dt.getTime()));
      pstmt.setInt(2, pulse);
      return pstmt.executeUpdate();
    }

    @Override
    public int insertPulseMeterTrend(String tbl, String dt, int pulse) throws SQLException {
      String sql = "INSERT INTO " + tbl + " VALUES('" + dt + "',?)";
      PreparedStatement pstmt = this.con.prepareStatement(sql);
      pstmt.setInt(1, pulse);
      return pstmt.executeUpdate();
    }

    @Override
    public ResultSet selectPulseMeterTrend(String tbl, Timestamp sdt, Timestamp edt) throws SQLException {
      String sql = "SELECT * FROM " + tbl + " WHERE SAVETIME>? AND SAVETIME<=?";
      PreparedStatement pstmt = this.con.prepareStatement(sql);
      pstmt.setTimestamp(1, sdt);
      pstmt.setTimestamp(2, edt);
      ResultSet rs = pstmt.executeQuery();
      return rs;
    }

    @Override
    public ResultSet selectPulseMeterTrend(String tbl, Date sdt, Date edt) throws SQLException {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
      Timestamp stm = new Timestamp(sdt.getTime());
      Timestamp etm = new Timestamp(edt.getTime());

      String sql = "SELECT * FROM " + tbl + " WHERE SAVETIME>? AND SAVETIME<=?";
      PreparedStatement pstmt = this.con.prepareStatement(sql);
      pstmt.setTimestamp(1, stm);
      pstmt.setTimestamp(2, etm);
      ResultSet rs = pstmt.executeQuery();
      return rs;
    }

    @Override
    public ResultSet selectPulseMeterTrend(String tbl, String sdt, String edt) throws SQLException {
      String sql = "SELECT * FROM " + tbl + " WHERE SAVETIME > '" + sdt + "' AND SAVETIME <= '" + edt + "'";
      PreparedStatement pstmt = this.con.prepareStatement(sql);
      ResultSet rs = pstmt.executeQuery();
      return rs;
    }

    @Override
    public ResultSet selectPulseMeterTrend(String tbl, Date dt) throws SQLException {
      String sql = "SELECT * FROM " + tbl + " WHERE SAVETIME = ?";
      PreparedStatement pstmt = this.con.prepareCall(sql);
      pstmt.setTimestamp(1, new Timestamp(dt.getTime()));
      ResultSet rs = pstmt.executeQuery();
      return rs;
    }

    @Override
    public ResultSet selectPulseMeterTrend(String tbl, String dt) throws SQLException {
      String sql = "SELECT * FROM " + tbl + " WHERE SAVETIME = '" + dt + "'";
      PreparedStatement pstmt = this.con.prepareCall(sql);
      ResultSet rs = pstmt.executeQuery();
      return rs;
    }
    
    public int deletePulseMeterTrend(String tbl, Date dt) throws SQLException {
        String sql = "DELETE FROM " + tbl + " WHERE SAVETIME < ?";
        PreparedStatement pstmt = this.con.prepareCall(sql);
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        cal.add(Calendar.DATE, -40);
        pstmt.setTimestamp(1, new Timestamp(cal.getTime().getTime()));
        return pstmt.executeUpdate();
    }
}   

