/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbcmodules;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author ccm
 */
public interface JDBCPulseMeterSelect {
    public ResultSet selectPulseMeterTrend(String tbl, Timestamp sdt, Timestamp edt) throws SQLException;
    public ResultSet selectPulseMeterTrend(String tbl, Date sdt, Date edt) throws SQLException;
    public ResultSet selectPulseMeterTrend(String tbl, String sdt, String edt) throws SQLException;
    public ResultSet selectPulseMeterTrend(String tbl, Date dt) throws SQLException;
    public ResultSet selectPulseMeterTrend(String tbl, String dt) throws SQLException;
}
