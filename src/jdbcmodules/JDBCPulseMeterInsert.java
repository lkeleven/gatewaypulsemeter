/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbcmodules;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author ccm
 */
public interface JDBCPulseMeterInsert {
    public int insertPulseMeterTrend(String tbl, Timestamp dt, int pulse)  throws SQLException;
    public int insertPulseMeterTrend(String tbl, Date dt, int pulse) throws SQLException;
    public int insertPulseMeterTrend(String tbl, String dt, int pulse) throws SQLException;
}
