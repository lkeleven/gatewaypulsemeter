/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbcmodules;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author ccm
 */
public class JDBCDerbyConnection {
    
    private Connection con;
    public Connection getDBConnection() {
        return con;
    }
    
    private String dburl;
    public String getDburl()
    {
      return this.dburl;
    }
    public void setDburl(String dburl) {
      this.dburl = dburl;
    }
    
    private String user;
    public String getUser() {
      return this.user;
    }
    public void setUser(String user) {
      this.user = user;
    }

    private String passwd;
    public String getPasswd() {
      return this.passwd;
    }
    public void setPasswd(String passwd) {
      this.passwd = passwd;
    }

    public Connection dbConnection() throws ClassNotFoundException, SQLException
    {
      return dbConnection(this.dburl, this.user, this.passwd);
    }

    public Connection dbConnection(String dburl, String user, String passwd) throws ClassNotFoundException, SQLException {
      String driver = "jdbc:derby:";
      Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
      if ((user == null) || (passwd == null))
        this.con = DriverManager.getConnection(driver + dburl);
      else
        this.con = DriverManager.getConnection(driver + dburl, user, passwd);
      
      return con;
    }
    
    public void dbDisconnection() throws SQLException {
        con.close();
    }
}
