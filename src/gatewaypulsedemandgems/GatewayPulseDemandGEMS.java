/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gatewaypulsedemandgems;

import gatewaysingleton.GatewayPulseMeterSingleton;
import java.text.SimpleDateFormat;
import java.util.Date;
import logback.LogBackLogger;

/**
 *
 * @author ccm
 */
public class GatewayPulseDemandGEMS {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        LogBackLogger.getInstance().getInfoLogger().info("   ### Gateway Pulse Meter Start!!! - " + df.format(new Date())  + " ###");
        LogBackLogger.getInstance().getSTDOUTLogger().debug("   ### Gateway Pulse Meter Start!!! - " + df.format(new Date())  + " ###");
        
        GatewayPulseMeterSingleton gatewaySingletone = GatewayPulseMeterSingleton.getGatewaySingleton();
        gatewaySingletone.GatewayPulseMeterStart();
        
        //Scanner sc = new Scanner(System.in);
        while (true) {
            try {
                //String cmd = sc.nextLine();
//            if (cmd.equals("exit")) {
//                break;
//            }
                Thread.sleep(60000);
            } catch (InterruptedException ex) {
                LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//                Logger.getLogger(GatewayPulseDemandGEMS_Ver1_1_1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
