package gpioreader;

public abstract interface PulseDetecting
{
  public abstract void PulseDetect();
}
