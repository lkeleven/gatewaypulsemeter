/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gpioreader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import logback.LogBackLogger;

/**
 *
 * @author ccm
 */
public class GPIOPulseMeterThd extends Thread {
    private GPIOPulseMeterListener pmListener = null;
    public void setGPIOPulseMeterListener(GPIOPulseMeterListener listener) {
        pmListener = listener;
    }
    
    
    private final PulseValueList _minPulseList = new PulseValueList();
    private final PulseValueList _5minPulseList = new PulseValueList();
            
    
    private DetectChange pulseDC = null;
    private void startPulseDetector() {
        pulseDC = new DetectChange();
        pulseDC.addEOIDetecting(eoi);
        pulseDC.addPulseDetecting(pulseDetector);
        pulseDC.addMeterDoorDetecting(doorDetector);
        pulseDC.start();
        
    }
    
    
    private void initPulseCount() {
        _pulseCount = 0;
                
        _secPulseCount = 0;
        _minPulseCount = 0;
        _5minPulseCount = 0;
        _15minPulseCount = 0;                
        
        minCount = 0;
        
        beforeSecPulseCount = 0;
        beforeMinPulseCount = 0;
    }
    
    private int _pulseCount;
    private final PulseDetecting pulseDetector = new PulseDetecting() {
        @Override
        public void PulseDetect() {
            _pulseCount = pulseDC.getPulseCount();
        }
    };

    private boolean eoiflag = false;
    private boolean startFlag = false;
    private final EOIDetecting eoi = new EOIDetecting() {
        @Override
        public void EODDetect() {
            eoiflag = true;
            
            if (pmListener != null)  // EOI 가 들어옴을 알려준다.
                pmListener.onEOIDetect(new Date());
            
            if (startFlag) {
                try {
                    minCount++;
                    Date dt = new Date();
//                    updatePulseCountEvent(dt,_pulseCount);
                    updatePulseCount();
                    Thread.sleep(150);
                } catch (ParseException | InterruptedException ex) {
                    LogBackLogger.getInstance().getExceptionLogger().debug("EOI Exception :: " + ex.getLocalizedMessage());
                }
            }
            
            if (!startFlag) {  // EOI의 최초 진입점.
                startFlag = true;
            } 
            
            initPulseCount();
            pulseDC.resetPulseCount(); // Pulse Detecter를 Init 한다.
            initMinFlags();

            eoiflag = false;
        }
    };
    
    private final MeterDoorDetecting doorDetector = new MeterDoorDetecting() {
        @Override
        public void meterDoorDetect() {
            if (pmListener != null) 
                pmListener.onMeterDoorOpen(new Date());                 
        }
    };
    
    
    @Override
    public void run() {
        startPulseDetector();        
        while (true) {
            if (startFlag) {
                try {
                    if (!eoiflag)
                        updatePulseCount();
                } catch (ParseException ex) {
                    Logger.getLogger(GPIOPulseMeterThd.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(GPIOPulseMeterThd.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private int _secPulseCount;
    private int _minPulseCount;
    private int _5minPulseCount;
    private int _15minPulseCount;
    private int beforeSecPulseCount;
    private int beforeMinPulseCount;

    
    private int nowPulseCount;
    private void updatePulseCount() throws ParseException {
        nowPulseCount = _pulseCount;
        _secPulseCount = nowPulseCount - beforeSecPulseCount;
        _minPulseCount = nowPulseCount - beforeMinPulseCount;
        
        Date dt = new Date();
        updatePulseCountEvent(dt,nowPulseCount);
    }
    
    private boolean _minflag = false;
    private boolean _5minflag = false;
    private boolean _15minflag = false;
    private void initMinFlags() {
        _minflag = false;
        _5minflag = false;
        _15minflag = false;
    }
    
    private int minCount;
 
    private void updatePulseCountEvent(Date dt, int nowPulseCount) throws ParseException {
        Calendar car = Calendar.getInstance();
        car.setTime(dt);
        
        updatePulseCountSec(dt, _secPulseCount);
        beforeSecPulseCount = nowPulseCount;
        
        if (minCount == 14) {
            _minflag = true;
            return;
        } 
        
        if ( car.get(Calendar.SECOND) <= 3) {
            if (_minflag) {
                _minPulseList.AddPulseCount(_minPulseCount);
                updatePulseCountMin(dt, _minPulseCount);
                beforeMinPulseCount = nowPulseCount;
                minCount++;
                //LogBackLogger.getInstance().getSTDOUTLogger().info("MinCount : " + minCount);
                _minflag = false;
            }
        } else {
            _minflag = true;
        }
        
        if ( (car.get(Calendar.MINUTE) % 5) == 0) {
            if (_5minflag) {
                _5minPulseCount = _minPulseList.sumPulseCount();
                updatePulseCount5Min(dt, _5minPulseCount);
                _5minPulseList.AddPulseCount(_5minPulseCount);
  
                _5minflag = false;
            }
        } else {
            _5minflag = true;
        }
        
        if ( (car.get(Calendar.MINUTE) % 15) == 0) {
            if (_15minflag) {
                _15minPulseCount = _5minPulseList.sumPulseCount();
                updatePulseCount15Min(dt, _15minPulseCount);

                _15minflag = false;
            }
        } else {
            _15minflag = true;
        }
    }
    
    private void updatePulseCountSec(Date dt, int pulseCount) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HHmmss");
        Calendar car = Calendar.getInstance();
        car.setTime(dt);
        if (pmListener != null)
            pmListener.onSecPulsed(sdf.parse( sdf.format(car.getTime() )), pulseCount);
    }
    private void updatePulseCountMin(Date dt, int pulseCount) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HHmm");
        Calendar car = Calendar.getInstance();
        car.setTime(dt);
        if (pmListener != null) {
                pmListener.onMinPulsed(sdf.parse( sdf.format(car.getTime() )), pulseCount);
        }
    }
    private void updatePulseCount5Min(Date dt, int pulseCount) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HHmm");
        Calendar car = Calendar.getInstance();
        car.setTime(dt);
        if (pmListener != null) {
                pmListener.on5MinPulsed(sdf.parse( sdf.format(car.getTime() )), pulseCount);
        }   
    }
    private void updatePulseCount15Min(Date dt, int pulseCount) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HHmm");
        Calendar car = Calendar.getInstance();
        car.setTime(dt);
        if (pmListener != null) {
            pmListener.on15MinPulsed(sdf.parse( sdf.format(car.getTime() )), pulseCount);
        }
    }
    
    
    private boolean checkOverDateTime(Date dt, Date nt) {
        Calendar dtCal = Calendar.getInstance();
        Calendar ntCal = Calendar.getInstance();        
        dtCal.setTime(dt);
        ntCal.setTime(nt);
        int dtHour = dtCal.get(Calendar.HOUR_OF_DAY);
        int dtMin = dtCal.get(Calendar.MINUTE);
        int ntHour = ntCal.get(Calendar.HOUR_OF_DAY);
        int ntMin = ntCal.get(Calendar.MINUTE);
            
            
        int diffHour = dtHour - ntHour;
        int diffMin = dtMin - ntMin;
        return !( (diffHour == 0) & (diffMin == 0) );
    }
    
    
    private void logPulseList(String tt, PulseValueList list) {
        System.out.println(tt);
        for (int nn : list.getPulseList()) {
            System.out.println(" :: " + nn);
        }
    }
    
    public class PulseValueList {
        private final ArrayList<Integer> pulseList = new ArrayList<>();
        public ArrayList<Integer> getPulseList() {
            return pulseList;
        }
        
        public void AddPulseCount(int pulse) {
            pulseList.add(pulse);
        }
        public int sumPulseCount() {
            int sum = 0;
            for (int nn : pulseList) {
                sum += nn;
            }
            pulseList.clear();
            return sum;
        }
        public void clear() {
            pulseList.clear();
        }
    }
    
}
