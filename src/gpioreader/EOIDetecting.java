package gpioreader;

public abstract interface EOIDetecting
{
  public abstract void EODDetect();
}
