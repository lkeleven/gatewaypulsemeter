package gpioreader;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListener;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import logback.LogBackLogger;

public class DetectChange extends Thread
{
    private ArrayList<EOIDetecting> eois = new ArrayList();
    private ArrayList<PulseDetecting> pulses = new ArrayList();
    private ArrayList<MeterDoorDetecting> doors = new ArrayList();

    private int pulseCount = 0;

    public void addEOIDetecting(EOIDetecting eoi)
    {
      this.eois.add(eoi);
    }

    public void addPulseDetecting(PulseDetecting pulseDetector) {
      this.pulses.add(pulseDetector);
    }

    public void addMeterDoorDetecting(MeterDoorDetecting doorDetector) {
        this.doors.add(doorDetector);
    }
    
    public int getPulseCount()
    {
      return this.pulseCount;
    }

    public void resetPulseCount() {
      this.pulseCount = 0;
    }

    public DetectChange() {
        GpioController gpio = GpioFactory.getInstance();

        GpioPinDigitalInput pulse = gpio.provisionDigitalInputPin(RaspiPin.GPIO_06, PinPullResistance.PULL_DOWN);
        pulse.addListener(new GpioPinListener[] { new GpioPinListenerDigital() {
            @Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent gpdsce) {
                if (gpdsce.getState() == PinState.LOW) {
                    ++pulseCount;  
                    for (PulseDetecting pulseDetector : DetectChange.this.pulses)
                        pulseDetector.PulseDetect();
                }
            }
            }
        });

        GpioPinDigitalInput eoi = gpio.provisionDigitalInputPin(RaspiPin.GPIO_25, PinPullResistance.PULL_DOWN);

        eoi.addListener(new GpioPinListener[] { new GpioPinListenerDigital() {
            @Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent gpdsce) {
                if (gpdsce.getState() == PinState.LOW) {
    //                pulseCount = 0;
                    for (EOIDetecting eoi : DetectChange.this.eois)
                        eoi.EODDetect(); 
                }
            }
            }
        });
        
        GpioPinDigitalInput door = gpio.provisionDigitalInputPin(RaspiPin.GPIO_02, PinPullResistance.PULL_DOWN);
        
        door.addListener(new GpioPinListener[] { new GpioPinListenerDigital() {
            @Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent gpdsce) {
                if (gpdsce.getState() == PinState.LOW) {
                    for (MeterDoorDetecting door : DetectChange.this.doors)
                        door.meterDoorDetect();
                }
            }
            }
        });
        
    }

    @Override
    public void run() {
        while(!Thread.currentThread().isInterrupted()) {
            try {
                    Thread.sleep(500L);
            } catch (InterruptedException ex) {
                LogBackLogger.getInstance().getExceptionLogger().debug(ex.getMessage());
//                Logger.getLogger(DetectChange.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
//        while (true) try {
//            Thread.sleep(500L);
//
//            continue;
//        }
//        catch (InterruptedException ex) {
//            Logger.getLogger(DetectChange.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

}