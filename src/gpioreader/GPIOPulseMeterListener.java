/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gpioreader;

import java.util.Date;

/**
 *
 * @author ccm
 */
public interface GPIOPulseMeterListener {
    public void onSecPulsed(Date dt, int pulseCount);
    public void onMinPulsed(Date dt, int pulseCount);
    public void on5MinPulsed(Date dt, int pulseCount);
    public void on15MinPulsed(Date dt, int pulseCount);
    
    public void onMeterDoorOpen(Date dt);
    
    public void onEOIDetect(Date dt);
}
