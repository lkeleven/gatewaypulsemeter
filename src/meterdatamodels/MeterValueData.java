/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package meterdatamodels;

import gemsprotocol.GEMSProtocolTypes;
import java.util.Date;

/**
 *
 * @author ccm
 */
public class MeterValueData {
    private float pulsePCT;
    public void setPulsePCT(float pct) {
        pulsePCT = pct;
    }
    public float getPulsePCT() {
        return pulsePCT;
    }
    
    private int pulseConst;
    public void setPulseConst(int value) {
        pulseConst = value;
    }
    public int getPulseConst() {
        return pulseConst;
    }
    
    private GEMSProtocolTypes.MeterDataType mdt;
    public void setMeterDataType(GEMSProtocolTypes.MeterDataType type) {
        mdt = type;
    }
    public GEMSProtocolTypes.MeterDataType getMeterDataType() {
        return mdt;
    }
    
    private int pulseCount;
    public void setPulseCount(int count, Date dt) {
        pulseCount = count;
        datas.addMeterRealData(pulseCount, dt);
    }
    public int getPulseCount() {
        return pulseCount;
    }
    
    private final MeterRealDatas datas = new MeterRealDatas();
    public MeterRealDatas getMeterRealDatas() {
        return datas;
    }
    
    public MeterValueData() {
        datas.setMaxSize(10);
    }
    
}
