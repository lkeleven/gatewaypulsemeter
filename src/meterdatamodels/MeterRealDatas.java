/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package meterdatamodels;

import gemsprotocol.GEMSProtocolTypes.MeterDataType;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author ccm
 */
public class MeterRealDatas {
    private MeterDataType mdt;
    public void setMeterDataType(MeterDataType type) {
        mdt = type;
    }
    public MeterDataType getMeterDataType() {
        return mdt;
    }

    private int maxSize;
    public void setMaxSize(int size) {
        maxSize = size;
    }
    public int getMaxSize() {
        return maxSize;
    }

    private final ArrayList<MeterRealData> arrayList;
    public MeterRealDatas() {
        arrayList = new ArrayList<>();
        mdt = MeterDataType.mdt1Min;
        maxSize = 10;
    }

    public int getDataCount() {
        return arrayList.size();
    }
    
    public MeterRealData getMeterRealData(int index) {
        MeterRealData mrd = null;
        if (arrayList.size() > index) {
            mrd = arrayList.get(index);
        }   
        return mrd;
    }

    public void addMeterRealData(int v, Date dt) {
        if (checkOverDateTime(dt)) {
            MeterRealData mv = new MeterRealData();
            mv.setPulseCount(v);
            mv.setPulseDate(dt);

            arrayList.add(0,mv);

            int size = arrayList.size();
            if (size > maxSize) {
                arrayList.remove(size-1);
            }
        }
    }
    
    private boolean checkOverDateTime(Date dt) {
        
        Calendar dtCal = Calendar.getInstance();
        Calendar ntCal = Calendar.getInstance();        
        dtCal.setTime(dt);
        int dtHour = dtCal.get(Calendar.HOUR_OF_DAY);
        int dtMin = dtCal.get(Calendar.MINUTE);
        
        for (MeterRealData mrd : arrayList) {
            ntCal.setTime(mrd.getPulsedate());
            int ntHour = ntCal.get(Calendar.HOUR_OF_DAY);
            int ntMin = ntCal.get(Calendar.MINUTE);
            
            int diffHour = dtHour - ntHour;
            int diffMin = dtMin - ntMin;
            if ( (diffHour == 0) & (diffMin == 0) ) {
                return false;
            }
        }
        return true;
    }
    
    public void listClear() {
        arrayList.clear();
    }
    
    public class MeterRealData {
        private int pulseCount;
        public void setPulseCount(int count) {
            pulseCount = count;
        }
        public int getPulseCount() {
            return pulseCount;
        }
        
        private Date pulseDate;
        public void setPulseDate(Date date) {
            pulseDate = date;
        }
        public Date getPulsedate() {
            return pulseDate;
        }
    }      
        
    
}
