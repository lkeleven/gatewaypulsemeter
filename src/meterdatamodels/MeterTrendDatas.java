/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package meterdatamodels;

import gemsprotocol.GEMSProtocolTypes.MeterDataType;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 *
 * @author ccm
 */
public class MeterTrendDatas {
    private MeterDataType mdt;
    public void setMeterDataType(MeterDataType type) {
        mdt = type;
    }
    public MeterDataType getMeterDataType() {
        return mdt;
    }

    private final ArrayList<MeterTrendData> arrayList;
    public MeterTrendDatas() {
        arrayList = new ArrayList<>();
        mdt = MeterDataType.mdt1Min;
    }

    public int getDataCount() {
        return arrayList.size();
    }
    
    public MeterTrendData getMeterRealData(int index) {
        MeterTrendData mrd = null;
        if (arrayList.size() > index) {
            mrd = arrayList.get(index);
        }   
        return mrd;
    }

    public void addMeterRealData(int v, Date dt) {
        MeterTrendData mv = new MeterTrendData();
        mv.setPulseCount(v);
        mv.setPulseDate(dt);

        arrayList.add(mv);
    }

    public void listClear() {
        arrayList.clear();
    }
    
    public class MeterTrendData {
        private int pulseCount;
        public void setPulseCount(int count) {
            pulseCount = count;
        }
        public int getPulseCount() {
            return pulseCount;
        }
        
        private Date pulseDate;
        public void setPulseDate(Date date) {
            pulseDate = date;
        }
        public Date getPulsedate() {
            return pulseDate;
        }
    }       
}
